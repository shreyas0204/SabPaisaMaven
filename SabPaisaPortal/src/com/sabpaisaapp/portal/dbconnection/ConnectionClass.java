package com.sabpaisaapp.portal.dbconnection;

import org.hibernate.SessionFactory;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;

public class ConnectionClass {
	public static SessionFactory sessionFactoryfactory = buildSessionFactory();
	public static SessionFactory sessionFactoryfactoryQcollect = buildSessionFactoryQcollect();

	public static SessionFactory buildSessionFactory() {

		if (sessionFactoryfactory == null) {
			Configuration configuration = new Configuration().configure();

			StandardServiceRegistryBuilder builder = new StandardServiceRegistryBuilder().applySettings(configuration
					.getProperties());
			SessionFactory factory = configuration.buildSessionFactory(builder.build());
			return factory;

		} else {
			return sessionFactoryfactory;
		}

	}
	public static SessionFactory buildSessionFactoryQcollect() {

		if (sessionFactoryfactoryQcollect == null) {
			Configuration configuration1 = new Configuration().configure("hibernate.qcollect.xml");

			StandardServiceRegistryBuilder builder = new StandardServiceRegistryBuilder()
					.applySettings(configuration1.getProperties());

			SessionFactory factory1 = configuration1.buildSessionFactory(builder.build());
			return factory1;

		} else {
			return sessionFactoryfactoryQcollect;
		}

	}
	public static SessionFactory getSessionFactoryfactoryQcollect() {
		return sessionFactoryfactoryQcollect;
	}

	public static SessionFactory getFactory() {

		return sessionFactoryfactory;
	}

	public static void shutDown() {

		getFactory().close();
		getSessionFactoryfactoryQcollect().close();
	}
}
