package com.sabpaisaapp.portal.dbconnection;

import java.sql.Connection;
import java.sql.DriverManager;
import java.util.Properties;

import org.apache.log4j.Logger;

import com.sabpaisaapp.portal.properties.LocationProperties;
import com.sabpaisaapp.portal.properties.SabpaisaAppPortalProperties;




public class DBConnection {
	Logger log = Logger.getLogger(DBConnection.class);
	
	LocationProperties location = new LocationProperties();
	String fileToBeReaded = location.getPropFileName();
	SabpaisaAppPortalProperties feeDeskProperties = new SabpaisaAppPortalProperties();
	Properties properties = feeDeskProperties.getPropValues(fileToBeReaded);
	int numberofRecordPerPage = Integer.parseInt(properties.getProperty("numberofRecordPerPage"));

	public Connection getConnection() {
		Connection con = null;
		String ip = properties.getProperty("host");
		String port = properties.getProperty("port");
		String dbName = properties.getProperty("dbName");
		String username = properties.getProperty("userName");
		String password = properties.getProperty("password");
		String DB_URL = "jdbc:mysql://" + ip + ":" + port + "/" + dbName + "";
		log.info("DB_URL == " + DB_URL);
		try {
			Class.forName("com.mysql.jdbc.Driver");
		} catch (ClassNotFoundException e) {

			e.printStackTrace();
		}

		try {
			con = DriverManager.getConnection(DB_URL, username, password);
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return con;
	}
}
