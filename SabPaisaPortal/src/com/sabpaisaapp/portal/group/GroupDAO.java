package com.sabpaisaapp.portal.group;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.hibernate.SessionFactory;

import com.login.LoginBean;
import com.sabpaisaapp.portal.dbconnection.ConnectionClass;
import com.sabpaisaapp.portal.dbconnection.DBConnection;
import com.sabpaisaapp.portal.feed.FeedsBean;

public class GroupDAO {
	DBConnection connection = new DBConnection();
	Connection conn = null;
	Statement stmt = null;
	ResultSet rs = null;
	LoginBean loginBean = new LoginBean();
	FeedsBean feedBean = new FeedsBean();
	GroupBean groupBean = new GroupBean();
	public static SessionFactory factory = ConnectionClass.getSessionFactoryfactoryQcollect();
	private Date createdDate = new Date();

	public List<GroupBean> getGroupList() throws SQLException {
		List<GroupBean> list = new ArrayList<GroupBean>();
		conn = connection.getConnection();
		try {
			stmt = conn.createStatement();
			String sql = "SELECT og.groupId,og.groupName,og.groupDescription,og.groupType,og.organization_id_fk,gc.comment_text,gc.comment_date FROM sabpaisaportal.organization_social_groups og INNER JOIN sabpaisaportal.group_comments gc ON og.groupId=gc.group_id";
			System.out.println("SQL =" + sql);
			rs = stmt.executeQuery(sql);
			GroupBean bean = null;
			while (rs.next()) {
				bean = new GroupBean();
				bean.setGroupId(rs.getInt("groupId"));
				bean.setGroupName(rs.getString("groupName"));
				bean.setGroupDescription(rs.getString("groupDescription"));
				// bean.setCreatedDate(rs.getTimestamp("createdDate"));
				bean.setGroupType(rs.getString("groupType"));
				bean.setOrganization_id_fk(rs.getInt("organization_id_fk"));
				// bean.setGroupImg(rs.getBytes("groupImg"));
				bean.setGroupComment(rs.getString("comment_text"));
				// bean.setCommcreatedDate(rs.getTimestamp("commcreatedDate"));
				list.add(bean);
			}
			return list;
		} catch (Exception e) {
			e.printStackTrace();
			return list;
		} finally {
			conn.close();
		}

	}

	public Integer saveGroupDetails(String groupName, String groupType, String groupDesc, Integer orgID,
			Date createdDate2) throws SQLException {
		conn = connection.getConnection();
		Date d = Calendar.getInstance().getTime(); // Current time
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm"); // Set your date format
		String currentData = sdf.format(d);
		try {
			String sql = "INSERT INTO sabpaisaportal.organization_social_groups(createdDate,groupDescription,groupImg,groupName,groupType,organization_id_fk,created_Date) "
					+ "VALUES('"+currentData+"','"+groupDesc+"',null,'"+groupName+"','"+groupType+"','"+orgID+"','"+currentData+"')";
			stmt = conn.createStatement();
			stmt.executeUpdate(sql);
		} catch (Exception e) {
			e.printStackTrace();
		}finally{
			conn.close();
		}
		return null;
	}

}
