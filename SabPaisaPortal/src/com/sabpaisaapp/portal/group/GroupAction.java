package com.sabpaisaapp.portal.group;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import java.util.ListIterator;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.apache.struts2.ServletActionContext;

import java.util.List;

import com.login.LoginBean;
import com.opensymphony.xwork2.ActionSupport;
import com.sabpaisaapp.portal.feed.FeedAction;
import com.sabpaisaapp.portal.feed.FeedsBean;

public class GroupAction extends ActionSupport {
	HttpServletRequest request = ServletActionContext.getRequest();
	HttpSession httpSession = request.getSession();
	HttpServletResponse response = ServletActionContext.getResponse();
	HttpSession sesssion = ServletActionContext.getRequest().getSession();
	static Logger log = Logger.getLogger(GroupAction.class.getName());
	LoginBean loginBean = new LoginBean();

	List<FeedsBean> listFeedsBean = new ArrayList<FeedsBean>();
	List<GroupBean> listGroupBean = new ArrayList<GroupBean>();
	GroupDAO groupDao = new GroupDAO();
	private Date createdDate = new Date();

	public String getGroupsList() throws SQLException {
		log.info("In side the Show Group List ACtion :::::::::::::::::::::::::");
		loginBean = (LoginBean) sesssion.getAttribute("adminLogin");
		try {
			listGroupBean = groupDao.getGroupList();
			log.info("Return List Size is :::::::::::::" + listGroupBean.size());
		} catch (Exception e) {
			e.printStackTrace();
		}

		return SUCCESS;
	}

	

	public String createGroups() throws SQLException {
		log.info("In side the Create Group List ACtion :::::::::::::::::::::::::");
		loginBean = (LoginBean) sesssion.getAttribute("adminLogin");
		String groupName = request.getParameter("groupName");
		String groupType = request.getParameter("groupType");
		String groupDesc = request.getParameter("GroupTxt");
		Integer orgID = loginBean.getOrg_Id_Fk();
		log.info("Group Created Date ::::::::::" + createdDate);
		log.info("Organizaton ID  ::::::::::" + orgID);
		Integer ID = groupDao.saveGroupDetails(groupName, groupType, groupDesc, orgID, createdDate);

		return SUCCESS;
	}

	public List<GroupBean> getListGroupBean() {
		return listGroupBean;
	}

	public void setListGroupBean(List<GroupBean> listGroupBean) {
		this.listGroupBean = listGroupBean;
	}
	public LoginBean getLoginBean() {
		return loginBean;
	}

	public void setLoginBean(LoginBean loginBean) {
		this.loginBean = loginBean;
	}
}
