package com.sabpaisaapp.portal.group;

import java.util.Date;

public class GroupBean {
	private Integer groupId;
	private String groupType;
	private String groupName;
	private String groupDescription;
	private byte[] groupImg;
	private Date createdDate = new Date();
	private Integer organization_id_fk;
	private String groupComment;
	private Date commcreatedDate = new Date();

	public String getGroupComment() {
		return groupComment;
	}

	public void setGroupComment(String groupComment) {
		this.groupComment = groupComment;
	}

	public Date getCommcreatedDate() {
		return commcreatedDate;
	}

	public void setCommcreatedDate(Date commcreatedDate) {
		this.commcreatedDate = commcreatedDate;
	}

	public Integer getGroupId() {
		return groupId;
	}

	public void setGroupId(Integer groupId) {
		this.groupId = groupId;
	}

	public String getGroupType() {
		return groupType;
	}

	public void setGroupType(String groupType) {
		this.groupType = groupType;
	}

	public String getGroupName() {
		return groupName;
	}

	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}

	public String getGroupDescription() {
		return groupDescription;
	}

	public void setGroupDescription(String groupDescription) {
		this.groupDescription = groupDescription;
	}

	public byte[] getGroupImg() {
		return groupImg;
	}

	public void setGroupImg(byte[] groupImg) {
		this.groupImg = groupImg;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public Integer getOrganization_id_fk() {
		return organization_id_fk;
	}

	public void setOrganization_id_fk(Integer organization_id_fk) {
		this.organization_id_fk = organization_id_fk;
	}

}
