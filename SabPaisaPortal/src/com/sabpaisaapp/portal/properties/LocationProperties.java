package com.sabpaisaapp.portal.properties;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class LocationProperties {
	String result = "";
	InputStream inputStream;

	public String getPropFileName() {

		Properties prop = new Properties();
		String fileToBeRead = "";
		try {

			String propFileName = "location.properties";
			
			inputStream = getClass().getClassLoader().getResourceAsStream(propFileName);

			if (inputStream != null) {
				prop.load(inputStream);
				String location = prop.getProperty("location");
				
				System.out.println("Location is =="+location);
				
				if (location.contentEquals("live")) {
					fileToBeRead = "SabpaisaAppPortalLive.properties";
				} else if (location.contentEquals("local")) {
					fileToBeRead = "SabpaisaAppPortalLocal.properties";
				} else if(location.contentEquals("uat")){
					fileToBeRead = "SabpaisaAppPortalUat.properties";
				}
				else if(location.contentEquals("QA")){
					fileToBeRead = "SabpaisaAppPortalQA.properties";
				}

			} else {
				throw new FileNotFoundException("property file '" + propFileName + "' not found in the classpath");
			}
		} catch (Exception e) {
			System.out.println("Exception: " + e);
		} finally {
			try {
				inputStream.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return fileToBeRead;
	}

}
