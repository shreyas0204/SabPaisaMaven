package com.sabpaisaapp.portal.properties;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class SabpaisaAppPortalProperties {
	
	String result = "";
	InputStream inputStream;
	
	public Properties getPropValues(String fileToBeReaded) {
		
		Properties prop = new Properties();
		try {
			
			//String propFileName = "FeeDesk.properties";
 
			inputStream = getClass().getClassLoader().getResourceAsStream(fileToBeReaded);
 
			if (inputStream != null) {
				prop.load(inputStream);
			} else {
				throw new FileNotFoundException("property file '" + fileToBeReaded + "' not found in the classpath");
			}
		} catch (Exception e) {
			System.out.println("Exception: " + e);
		} finally {
			try {
				inputStream.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return prop;
	}

}
