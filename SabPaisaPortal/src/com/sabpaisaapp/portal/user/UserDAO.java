package com.sabpaisaapp.portal.user;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import com.sabpaisaapp.portal.dbconnection.DBConnection;

public class UserDAO {
	DBConnection connection = new DBConnection();
	Connection conn = null;
	Statement stmt = null;
	ResultSet rs = null;
	UserBean bean = new UserBean();

	public UserBean getAdminDetails(Integer loginId) throws SQLException {
		conn = connection.getConnection();
		try {
			String sql = "SELECT * FROM sabpaisaportal.users_master where loginBean_loginId='" + loginId + "'";
			stmt = conn.createStatement();
			rs = stmt.executeQuery(sql);
			while (rs.next()) {
				bean.setFirstName(rs.getString("firstName"));
				bean.setLastName(rs.getString("lastName"));
				bean.setEmailID(rs.getString("emailID"));
				bean.setContactNumber(rs.getString("contactNumber"));
				bean.setAddress(rs.getString("address"));
				bean.setUserId(rs.getInt("userId"));
				bean.setUserImg(rs.getBytes("userImg"));

			}
			return bean;
		} catch (Exception e) {
			e.printStackTrace();
			return bean;
		} finally {
			conn.close();
		}

	}

}
