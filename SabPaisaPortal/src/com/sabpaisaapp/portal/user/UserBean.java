package com.sabpaisaapp.portal.user;

public class UserBean {
	private Integer userId;

	private String firstName, lastName, dateOfBirth, emailID, contactNumber, address, deviceID, uinNO, userType,
			userTokens, accessTokenFB, accessTokenGoogle;
	private byte[] userImg;

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getDateOfBirth() {
		return dateOfBirth;
	}

	public void setDateOfBirth(String dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}

	public String getEmailID() {
		return emailID;
	}

	public void setEmailID(String emailID) {
		this.emailID = emailID;
	}

	public String getContactNumber() {
		return contactNumber;
	}

	public void setContactNumber(String contactNumber) {
		this.contactNumber = contactNumber;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getDeviceID() {
		return deviceID;
	}

	public void setDeviceID(String deviceID) {
		this.deviceID = deviceID;
	}

	public String getUinNO() {
		return uinNO;
	}

	public void setUinNO(String uinNO) {
		this.uinNO = uinNO;
	}

	public String getUserType() {
		return userType;
	}

	public void setUserType(String userType) {
		this.userType = userType;
	}

	public String getUserTokens() {
		return userTokens;
	}

	public void setUserTokens(String userTokens) {
		this.userTokens = userTokens;
	}

	public String getAccessTokenFB() {
		return accessTokenFB;
	}

	public void setAccessTokenFB(String accessTokenFB) {
		this.accessTokenFB = accessTokenFB;
	}

	public String getAccessTokenGoogle() {
		return accessTokenGoogle;
	}

	public void setAccessTokenGoogle(String accessTokenGoogle) {
		this.accessTokenGoogle = accessTokenGoogle;
	}

	public byte[] getUserImg() {
		return userImg;
	}

	public void setUserImg(byte[] userImg) {
		this.userImg = userImg;
	}

}
