package com.sabpaisaapp.portal.feed;

public class FeedsBean {
	private Integer feedId;
	private String feed_Name;
	private String feedText;
	private String feed_date;
	private byte[] feedImg;
	private Integer organization_id_fk;
	private String comment;

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

	public Integer getOrganization_id_fk() {
		return organization_id_fk;
	}

	public void setOrganization_id_fk(Integer organization_id_fk) {
		this.organization_id_fk = organization_id_fk;
	}

	public Integer getFeedId() {
		return feedId;
	}

	public void setFeedId(Integer feedId) {
		this.feedId = feedId;
	}

	public String getFeed_Name() {
		return feed_Name;
	}

	public void setFeed_Name(String feed_Name) {
		this.feed_Name = feed_Name;
	}

	public String getFeedText() {
		return feedText;
	}

	public void setFeedText(String feedText) {
		this.feedText = feedText;
	}

	public String getFeed_date() {
		return feed_date;
	}

	public void setFeed_date(String feed_date) {
		this.feed_date = feed_date;
	}

	public byte[] getFeedImg() {
		return feedImg;
	}

	public void setFeedImg(byte[] feedImg) {
		this.feedImg = feedImg;
	}
}
