package com.sabpaisaapp.portal.feed;

import java.util.Date;

public class FCommentBean {
	private Integer commentId;
	private String  comment_text;
	private Date createdDate=new Date();
	private Integer comment_by;
	private Integer feed_id;
	public Integer getCommentId() {
		return commentId;
	}
	public void setCommentId(Integer commentId) {
		this.commentId = commentId;
	}
	public String getComment_text() {
		return comment_text;
	}
	public void setComment_text(String comment_text) {
		this.comment_text = comment_text;
	}
	public Date getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}
	public Integer getComment_by() {
		return comment_by;
	}
	public void setComment_by(Integer comment_by) {
		this.comment_by = comment_by;
	}
	public Integer getFeed_id() {
		return feed_id;
	}
	public void setFeed_id(Integer feed_id) {
		this.feed_id = feed_id;
	}
}
