package com.sabpaisaapp.portal.feed;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.NoSuchElementException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import java.util.List;

import org.apache.log4j.Logger;
import org.apache.struts2.ServletActionContext;


import com.login.LoginAction;
import com.login.LoginBean;
import com.mysql.fabric.xmlrpc.base.Array;
import com.opensymphony.xwork2.ActionSupport;

public class FeedAction extends ActionSupport {
	HttpServletRequest request = ServletActionContext.getRequest();
	HttpSession httpSession = request.getSession();
	HttpServletResponse response = ServletActionContext.getResponse();
	HttpSession sesssion = ServletActionContext.getRequest().getSession();
	static Logger log = Logger.getLogger(FeedAction.class.getName());
	LoginBean loginBean = new LoginBean();
	private FeedsBean feedBean = new FeedsBean();
	FeedsDAO feedDao = new FeedsDAO();
	List<FeedsBean> listFeedsBean = new ArrayList<FeedsBean>();

	public String saveFeedsDetails() throws SQLException {
		loginBean = (LoginBean) sesssion.getAttribute("adminLogin");
		String feedName = request.getParameter("feedName");
		String feedTxt = request.getParameter("FeedTxt");
		Integer orgID = loginBean.getOrg_Id_Fk();
		feedBean.setFeed_Name("feedName");
		feedBean.setFeedText("feedTxt");
		feedBean.setOrganization_id_fk(orgID);
		Integer id = feedDao.saveFeedsDetails(feedName, feedTxt, orgID);

		return SUCCESS;
	}

	public FeedsBean getFeedBean() {
		return feedBean;
	}

	public void setFeedBean(FeedsBean feedBean) {
		this.feedBean = feedBean;
	}

	public List<FeedsBean> getListFeedsBean() {
		return listFeedsBean;
	}

	public void setListFeedsBean(List<FeedsBean> listFeedsBean) {
		this.listFeedsBean = listFeedsBean;
	}

	public String getFeedsList() throws SQLException {
		log.info("In side the get feeds Details :::::::::::::::::::");
		listFeedsBean = feedDao.getAllFeedsList();
		log.info("list Size :::::::" + listFeedsBean.size());
		/*for (int i = 0; i < listFeedsBean.size(); i++) {
			FCommentBean bean=feedDao.getFeedsComment(listFeedsBean.get(i));
			LOG.info("Comment By Name ::: "+bean.getComment_text());
			listFeedsBean.get(i).setComment(bean.getComment_text());
		}*/
		return SUCCESS;
	}

	public LoginBean getLoginBean() {
		return loginBean;
	}

	public void setLoginBean(LoginBean loginBean) {
		this.loginBean = loginBean;
	}
}
