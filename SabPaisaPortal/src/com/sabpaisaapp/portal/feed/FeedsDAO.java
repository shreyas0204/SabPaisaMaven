package com.sabpaisaapp.portal.feed;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.mail.Session;

import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import com.login.LoginBean;
import com.mysql.fabric.xmlrpc.base.Array;
import com.sabpaisaapp.portal.dbconnection.ConnectionClass;
import com.sabpaisaapp.portal.dbconnection.DBConnection;

public class FeedsDAO {
	DBConnection connection = new DBConnection();
	Connection conn = null;
	Statement stmt = null;
	ResultSet rs = null;
	LoginBean loginBean = new LoginBean();
	FeedsBean feedBean = new FeedsBean();
	public static SessionFactory factory = ConnectionClass.getSessionFactoryfactoryQcollect();
	private Date createdDate = new Date();

	public Integer saveFeedsDetails(String feedName, String feedTxt, Integer orgID) throws SQLException {
		conn = connection.getConnection();
		try {
			String sql = "INSERT INTO sabpaisaportal.organization_feeds (feedImg,feedText,feed_Name,feed_date,organization_id_fk)"
					+ "values(null,'" + feedTxt + "','" + feedName + "','" + createdDate + "','" + orgID + "')";

			stmt = conn.createStatement();
			stmt.executeUpdate(sql);
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			conn.close();
		}
		return null;
	}

	public List<FeedsBean> getAllFeedsList() throws SQLException {
		List<FeedsBean> list = new ArrayList<FeedsBean>();
		conn = connection.getConnection();
		try {
			stmt = conn.createStatement();
			// String sql = "SELECT * FROM sabpaisaportal.organization_feeds";
			String sql = "SELECT of.feedId,of.feed_Name,of.feedText,of.feed_date,of.organization_id_fk,fc.comment_text  FROM sabpaisaportal.organization_feeds of INNER JOIN  sabpaisaportal.feeds_comments fc ON of.feedId=fc.feed_id";
			System.out.println("SQL =" + sql);
			rs = stmt.executeQuery(sql);
			FeedsBean bean = null;
			while (rs.next()) {
				bean = new FeedsBean();
				bean.setFeedId(rs.getInt("feedId"));
				bean.setFeed_Name(rs.getString("feed_Name"));
				bean.setFeedText(rs.getString("feedText"));
				bean.setFeed_date(rs.getString("feed_date"));
				//bean.setFeedImg(rs.getBytes("feedImg"));
				bean.setOrganization_id_fk(rs.getInt("organization_id_fk"));
				bean.setComment(rs.getString("comment_text"));
				list.add(bean);
			}
			return list;
		} catch (Exception e) {
			e.printStackTrace();
			return list;
		} finally {
			conn.close();
		}
	}

	public FCommentBean getFeedsComment(FeedsBean feedsBean) throws SQLException {
		conn = connection.getConnection();
		FCommentBean bean = null;
		try {
			String sql = "SELECT * FROM sabpaisaportal.feeds_comments where feed_id='" + feedsBean.getFeedId() + "'";
			stmt = conn.createStatement();
			System.out.println("SQL =" + sql);
			rs = stmt.executeQuery(sql);

			if (rs.next()) {
				bean = new FCommentBean();
				bean.setCommentId(rs.getInt("commentId"));
				bean.setComment_by(rs.getInt("comment_by"));
				bean.setComment_text(rs.getString("comment_text"));
				bean.setCreatedDate(rs.getTimestamp("createdDate"));
				bean.setFeed_id(rs.getInt("feed_id"));

			}
			return bean;
		} catch (Exception e) {
			e.printStackTrace();
			return bean;
		} finally {
			conn.close();
		}
	}

}
