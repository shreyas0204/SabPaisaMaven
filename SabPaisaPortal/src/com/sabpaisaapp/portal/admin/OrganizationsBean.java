package com.sabpaisaapp.portal.admin;

import javax.persistence.Column;

public class OrganizationsBean {
	private Integer organizationId;

	private String organization_name;
	private String relationship_type;
	private String loginBean_loginId;

	public Integer getOrganizationId() {
		return organizationId;
	}

	public void setOrganizationId(Integer organizationId) {
		this.organizationId = organizationId;
	}

	public String getOrganization_name() {
		return organization_name;
	}

	public void setOrganization_name(String organization_name) {
		this.organization_name = organization_name;
	}

	public String getRelationship_type() {
		return relationship_type;
	}

	public void setRelationship_type(String relationship_type) {
		this.relationship_type = relationship_type;
	}

	public String getLoginBean_loginId() {
		return loginBean_loginId;
	}

	public void setLoginBean_loginId(String loginBean_loginId) {
		this.loginBean_loginId = loginBean_loginId;
	}
}
