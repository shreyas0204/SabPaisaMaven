package com.sabpaisaapp.portal.upload;

import java.io.File;
import java.io.FileInputStream;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.Properties;
import java.util.Set;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.apache.struts2.ServletActionContext;

import COM.rsa.jsafe.ap;

import com.opensymphony.xwork2.ActionSupport;
import com.sabpaisaapp.portal.properties.LocationProperties;
import com.sabpaisaapp.portal.properties.SabpaisaAppPortalProperties;

public class AppAction extends ActionSupport {
	String folder = "E:\\SabpaisaAppPortal Uploded Files";
	LocationProperties location = new LocationProperties();
	String fileToBeReaded = location.getPropFileName();
	SabpaisaAppPortalProperties feeDeskProperties = new SabpaisaAppPortalProperties();
	Properties properties = feeDeskProperties.getPropValues(fileToBeReaded);
	int numberofRecordPerPage = Integer.parseInt(properties.getProperty("numberofRecordPerPage"));
	
	
	
	
	private static final long serialVersionUID = 1L;
	
	HttpServletRequest request = ServletActionContext.getRequest();
	HttpSession httpSession = request.getSession();
	HttpServletResponse response = ServletActionContext.getResponse();
	static Logger log = Logger.getLogger(AppAction.class.getName());
	
	String fileFileName;
	FileInputStream inputStream;
	private File fileUpload;
	private String fileUploadFileName;
	
	private String contentType;
	public AppDAO aplDAO = new AppDAO();
	
	public String uploadUin() throws Exception {

		if (fileUploadFileName.endsWith(".xlsx") || fileUploadFileName.endsWith(".csv")) {
			try {
			
				Integer totalFileSize = null;
				String ext = null;
				String dbpath = null;
				try {

					totalFileSize = new File(folder).listFiles().length;
					totalFileSize = totalFileSize + 1;
					ext = FilenameUtils.getExtension(fileUploadFileName);
					fileUploadFileName = StringUtils.removeEnd(fileUploadFileName, "." + ext);
					dbpath = folder + "\\" + fileUploadFileName + "_" + totalFileSize.toString() + "." + ext;

				} catch (Exception e) {
					dbpath = folder + "\\" + fileUploadFileName;
					e.printStackTrace();
				}
				File dstFile = new File(dbpath);
				new File(folder).mkdirs();
				// to save file in specified Location
				FileUtils.copyFile(fileUpload, dstFile);
				log.info("File Name is ::" + fileUploadFileName);
				try {
					try {
						String uploadFlag = aplDAO.generateTempTable(fileUpload, "", "", "", "");
						log.info("upload flag is ::" + uploadFlag);

						if (uploadFlag == "tableCreationError") {
							request.setAttribute("msg", "Some Headers Missing ,not able upload Record");
							return "failure";
						}

						if (uploadFlag == "excelError") {
							request.setAttribute("msg", "One or more cell headers missing values.");
							return "failure";
						}
						if (uploadFlag == "fail") {
							request.setAttribute("msg", "Please Check Fees Configuration if not properly mapped");
							return "failure";
						}

						if (uploadFlag == "dbError") {

							request.setAttribute("msg", "Data is not correct in file, please try again.");
							return "failure";

						} else {
							request.setAttribute("msg", "Student Record Uploaded Successfully");
							return SUCCESS;
						}

					} catch (NullPointerException e) {
						e.printStackTrace();
						request.setAttribute("msg", "Not able to upload file issue \n may be issue in configuration");
						return "failure";
					}

				} catch (Exception e) {
					e.printStackTrace();
					request.setAttribute("msg", "Error in file uploading, please try again.");
					return "failure";
				}

			} catch (Exception e) {
				e.printStackTrace();
				request.setAttribute("msg", "Error in file uploading, please try again.");
				return "failure";
			}

		}

		else {
			// log.info("file Format not Match");
			String msg = "Please Select Proper File Format";
			request.setAttribute("msg", msg);
			return "failure";
		}

	}

	// End of Action Methods



	public String getFileFileName() {
		return fileFileName;
	}

	public void setFileFileName(String fileFileName) {
		this.fileFileName = fileFileName;
	}

	public File getFileUpload() {
		return fileUpload;
	}

	public void setFileUpload(File fileUpload) {
		this.fileUpload = fileUpload;
	}

	public String getFileUploadFileName() {
		return fileUploadFileName;
	}

	public void setFileUploadFileName(String fileUploadFileName) {
		this.fileUploadFileName = fileUploadFileName;
	}

	public String getContentType() {
		return contentType;
	}

	public void setContentType(String contentType) {
		this.contentType = contentType;
	}

	public FileInputStream getInputStream() {
		return inputStream;
	}

	public void setInputStream(FileInputStream inputStream) {
		this.inputStream = inputStream;
	}

	/*public AffBean getAffBean() {
		return affBean;
	}

	public void setAffBean(AffBean affBean) {
		this.affBean = affBean;
	}*/

	
	
	

}
