package com.sabpaisaapp.portal.upload;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import java.util.regex.Pattern;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.mail.MessagingException;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.apache.struts2.ServletActionContext;
import org.hibernate.Criteria;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;




import com.sabpaisaapp.portal.dbconnection.ConnectionClass;
import com.sabpaisaapp.portal.dbconnection.DBConnection;

public class AppDAO {

	DBConnection connection = new DBConnection();
	public static SessionFactory factory = ConnectionClass.getFactory();
	static Logger log = Logger.getLogger(AppDAO.class.getName());
	private static final DateFormat sdf = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
	HttpSession ses = ServletActionContext.getRequest().getSession();
	String tableName = "users_master";

	Connection conn = null;
	   Statement stmt = null;
	
	public String generateTempTable(File fileUpload, String sendLink, String serviceId, String productId,
			String sheetName) throws Exception {

		File excelFile = fileUpload;
		String uploadFlag = null;

		FileInputStream fileInputStream = new FileInputStream(excelFile);

		XSSFWorkbook xssfWorkbook = new XSSFWorkbook(fileInputStream);

		XSSFSheet hssfSheet = xssfWorkbook.getSheetAt(0);
		Session session = factory.openSession();

		try {
			Iterator<Row> rows = hssfSheet.rowIterator();
			XSSFCell cell;
			ArrayList<String> columnList = new ArrayList<String>();

			while (rows.hasNext()) {
				XSSFRow row = (XSSFRow) rows.next();
				Iterator<Cell> cells = row.cellIterator();

				while (cells.hasNext()) {

					cell = (XSSFCell) cells.next();

					if (row.getRowNum() == 0) {

						// to get header name of each column of excel file to
						// create column name in table

						columnList.add(cell.getStringCellValue());

						continue;

					}

				}

			}
			log.info("size of columnlist is.. " + columnList.size());
			Integer excelColumnSize = columnList.size();
			List<String> useList = new ArrayList<String>();

			ses.setAttribute("sesColumnList", columnList);

			String dynSQL = new String();

			Iterator<String> iterator = columnList.iterator();

			while (iterator.hasNext()) {
				String string2 = (String) iterator.next();

				string2 = string2.replaceAll("\\s", "_");
				// creating parameter with its data types
				string2 = StringUtils.removeEnd(string2, "_");
				log.info("generate column names ::" + string2);
				dynSQL = dynSQL + "," + string2 + " varchar(255)";

			}

			String[] splitsqlString = dynSQL.split(",");
			for (int i = 0; i < splitsqlString.length; i++) {
				String string2 = splitsqlString[i];
				useList.add(string2);

			}
			Integer sheetNo = 1;
			dynSQL = dynSQL.substring(1, dynSQL.length());

			log.info("dynSQL is.." + dynSQL);
			Boolean CreateDBflag = false;
			Connection con = (Connection) connection.getConnection();
			DatabaseMetaData meta = con.getMetaData();
			ResultSet res = meta.getTables(null, null, tableName, null);
			System.out.println("No of Column is ==" + res.getRow());
			if (!res.next()) {

				// CreateDBflag = true;
				System.out.println("Table Not Exists");
			} else {
				CreateDBflag = false;

			}

			if (CreateDBflag == true) {

				try {
					session.createSQLQuery(
							"CREATE TABLE "
									+ tableName
									+ " (id int(5) NOT NULL PRIMARY KEY AUTO_INCREMENT,PRODUCT_TYPE varchar(10),SERVICE_TYPE varchar(10),"
									+ dynSQL
									+ ", SECRET_KEY varchar(50) UNIQUE,STATUS varchar(20),CREATED_DATE DATETIME,PAYMENT_TRANSACTION_DATE DATETIME,PAYMENT_TRANSACTION_ID varchar(55),PAID_AMOUNT DOUBLE,BASE_AMOUNT DOUBLE,CLIENT_ID varchar(50),SEND_LINK varchar(2),SHEET_NO varchar(50),UPLOADED_BY_USER varchar(5))")
							.executeUpdate();

					log.info("Table created");

				} catch (RuntimeException e) {
					return "tableCreationError";
				}

			}

			// importExcelFileToDatabase2(excelFile, useList);
			try {
				// ses.setAttribute("fieldValidationFlag",
				// firstTimeTemplateCreationFlag);
				uploadFlag = importExcelFileToDataBase(excelFile, useList, excelColumnSize, sendLink, serviceId,
						productId, tableName, sheetName);

				return uploadFlag;

			} catch (Exception e) {
				e.printStackTrace();
				return "fail";
			}
		} catch (Exception e) {
			e.printStackTrace();
			return "fail";
		}

	}

	// updated flow of Add student FD0.4
	@SuppressWarnings("null")
	public String importExcelFileToDataBase(File fileUpload, List<String> colList, Integer exceColSize,
			String sendLink, String serviceId, String productId, String tempTableName, String sheetName)
			throws IOException {
		Session session = factory.openSession();

		FileInputStream fileInputStream = new FileInputStream(fileUpload);
		XSSFWorkbook xssfWorkbook = new XSSFWorkbook(fileInputStream);
		XSSFSheet hssfSheet = xssfWorkbook.getSheetAt(0);

		try {

		

			Iterator<Row> rows = hssfSheet.rowIterator();
			String stringVal;
			String blankVal;
			String errorlVal;
			String booleanval;
			String defaultVal;

			Long numVal;
			XSSFCell cell = null;

			while (rows.hasNext()) {

				XSSFRow row = (XSSFRow) rows.next();

				if (row.getRowNum() == 0) {
					continue;
				}

				int i = 0;
				System.out.println("Empty Value of Cell is = " + hssfSheet.getRow(1).getCell(3));
				Iterator<Cell> cells = row.cellIterator();
				ArrayList<Object> tempArrayList = new ArrayList<Object>();
				while (i < exceColSize) {

					Cell c = row.getCell(i);
					System.out.println("fir chala mera loop");
					try {
						System.out.println("Cell value is =" + c.getStringCellValue());
						tempArrayList.add(c.getStringCellValue().trim());
					} catch (NullPointerException NE) {
						System.out.println("Cell number yahi wala dikkat diya at number:" + i + " - " + NE);
						tempArrayList.add("");
					}
					i++;
				}

				/*
				 * 
				 * while (cells.hasNext()|| i<exceColSize) {
				 * 
				 * 
				 * 
				 * System.out.println("iteration number is " +i);
				 * 
				 * 
				 * try {
				 * 
				 * cell = (XSSFCell) cells.next();
				 * 
				 * System.out.println("All Cell Value with position "+i); }
				 * catch (NoSuchElementException e) { System.out.println(
				 * "exception is.." + e);
				 * 
				 * System.out.println("inside is cell == null ");
				 * //tempArrayList.add("NA");
				 * 
				 * 
				 * if (cell == null) { cell.setCellValue("anish");
				 * System.out.println("Inside Null"); }
				 * 
				 * 
				 * 
				 * switch (cell.getCellType()) { case
				 * XSSFCell.CELL_TYPE_BOOLEAN: System.out.println(
				 * "Inside CELL_TYPE_BOOLEAN"); cell.setCellValue(0); case
				 * XSSFCell.CELL_TYPE_ERROR: System.out.println(
				 * "Inside CELL_TYPE_ERROR"); cell.setCellValue(0); case
				 * XSSFCell.CELL_TYPE_FORMULA: System.out.println(
				 * "Inside CELL_TYPE_FORMULA"); cell.setCellValue(0); case
				 * XSSFCell.CELL_TYPE_NUMERIC: System.out.println(
				 * "Inside CELL_TYPE_NUMERIC"); cell.setCellValue(0); case
				 * XSSFCell.CELL_TYPE_STRING: cell.setCellValue("NA"); case
				 * XSSFCell.CELL_TYPE_BLANK: cell.setCellValue("NA"); }
				 * 
				 * }catch (NullPointerException e) { if (cell == null) {
				 * System.out.println("Inside catch "+cell);
				 * 
				 * } }
				 * 
				 * switch (cell.getCellType()) {
				 * 
				 * case XSSFCell.CELL_TYPE_STRING: stringVal =
				 * cell.getStringCellValue(); System.out.println("1 = "
				 * +stringVal); stringVal = stringVal.replaceAll("\\u00A0", "");
				 * try { if (null == stringVal || stringVal.isEmpty()) {
				 * tempArrayList.add(""); } else {
				 * tempArrayList.add(stringVal.trim()); } } catch (Exception e)
				 * { tempArrayList.add(""); } break;
				 * 
				 * case XSSFCell.CELL_TYPE_NUMERIC: numVal = (long)
				 * cell.getNumericCellValue(); System.out.println("Value is = "
				 * +cell.getNumericCellValue()); String tempSt =
				 * numVal.toString().replaceAll("\\u00A0", "");
				 * System.out.println("2"); try { if (null == numVal ||
				 * "".equals(numVal)) {
				 * 
				 * tempArrayList.add(""); } else {
				 * 
				 * tempArrayList.add(tempSt.trim()); } } catch (Exception e) {
				 * tempArrayList.add(""); } break;
				 * 
				 * case XSSFCell.CELL_TYPE_BLANK:
				 * 
				 * blankVal = "NA"; tempArrayList.add(blankVal.trim());
				 * 
				 * break;
				 * 
				 * case XSSFCell.CELL_TYPE_ERROR: errorlVal =
				 * cell.getStringCellValue();
				 * errorlVal.toString().replaceAll("\\u00A0", "");
				 * System.out.println("4"); try { if (null == errorlVal ||
				 * errorlVal.isEmpty()) { tempArrayList.add(""); } else {
				 * tempArrayList.add(errorlVal.trim()); } } catch (Exception e)
				 * { tempArrayList.add(""); } break;
				 * 
				 * case XSSFCell.CELL_TYPE_BOOLEAN: booleanval =
				 * cell.getStringCellValue();
				 * booleanval.toString().replaceAll("\\u00A0", "");
				 * System.out.println("6"); try { if (null == booleanval ||
				 * booleanval.isEmpty()) { tempArrayList.add(""); } else {
				 * tempArrayList.add(booleanval.trim()); } } catch (Exception e)
				 * { tempArrayList.add(""); } break;
				 * 
				 * default: defaultVal = cell.getStringCellValue();
				 * defaultVal.toString().replaceAll("\\u00A0", "");
				 * System.out.println("7"); try { if (null == defaultVal ||
				 * defaultVal.isEmpty()) { tempArrayList.add(""); } else {
				 * tempArrayList.add(defaultVal.trim()); } } catch (Exception e)
				 * { tempArrayList.add(""); } break;
				 * 
				 * } i++; }
				 */

				log.info("Insert Value list size ::" + tempArrayList.size());
				log.info("Column list size ::" + exceColSize);
				if (!exceColSize.equals(tempArrayList.size())) {
					return "excelError";
				}
				String insertParam = new String();

				Iterator<Object> iterator = tempArrayList.iterator();

				while (iterator.hasNext()) {
					String ss = (String) iterator.next();

					insertParam = insertParam + "," + "'" + ss + "'";
				}
				String key = null;
				Transaction tx = session.beginTransaction();
				try {

					insertParam = insertParam.substring(1, insertParam.length());
					/*
					 * keygenerator id = new keygenerator(); try { key =
					 * id.createTransxId(); } catch (Exception e) { // TODO:
					 * handle exception key = id.anotherLinkKey(); }
					 */
					
					
					
					
					conn = connection.getConnection();
				      System.out.println("Connected database successfully...");
				      System.out.println("Inserting records into the table...");
				      stmt = conn.createStatement();
					
					
					

					Date date = new Date();
					log.info("inserting values ::" + insertParam);
					String sql = "INSERT INTO `sabpaisaportal`.`users_master`(`userId`,`address`,`contactNumber`,`dateOfBirth`,`emailID`,"
							+ "`firstName`,`lastName`,`organization_id_fk`,`loginBean_loginId`)VALUES"
							+ "(null,"+insertParam+",'1','1')";
					log.info("sql ::" + sql);
					
					int counter = stmt.executeUpdate(sql);
				}catch(SQLException se){
				      //Handle errors for JDBC
				      se.printStackTrace();
				   }catch(Exception e){
					   
						return "dbError";
				   }finally{
				      //finally block used to close resources
				      try{
				         if(stmt!=null)
				            conn.close();
				      }catch(SQLException se){
				      }// do nothing
				      try{
				         if(conn!=null)
				            conn.close();
				      }catch(SQLException se){
				         se.printStackTrace();
				      }//end finally try
				
					// return "success";
				   }
			}
		

			return "success";
		}

		catch (Exception e) {

			e.printStackTrace();
			return "fail";
		} finally {
			session.close();
		}

		// return null;

	}

}
