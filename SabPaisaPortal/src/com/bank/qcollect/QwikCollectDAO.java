package com.bank.qcollect;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.List;


import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;

import com.qcollect.beans.CollegeBean;
import com.qcollect.beans.CompanyBean;
import com.qcollect.beans.ServicesBean;
import com.qcollect.beans.StateBean;
import com.qcollect.beans.SuperAdminBean;
import com.qcollect.beans.BankDetailsBean;
import com.qcollect.beans.BeanFormDetails;
import com.qcollect.beans.ClientMappingCodeOfSabPaisa;
import com.sabpaisaapp.portal.dbconnection.ConnectionClass;

public class QwikCollectDAO {

	public static SessionFactory factory = ConnectionClass.getSessionFactoryfactoryQcollect();
	static Logger log = Logger.getLogger(QwikCollectDAO.class.getName());

	public BankDetailsBean getBankDetails(Integer id) {
		Session session = factory.openSession();
		BankDetailsBean bankBean = null;
		try {
			bankBean = (BankDetailsBean) session.get(BankDetailsBean.class, id);
			return bankBean;
		} catch (Exception ex) {
			ex.printStackTrace();
			return bankBean;
		} finally {
			session.close();
		}
	}

	public List<BeanFormDetails> getForms(List<String> clientList,String status) {

		List<BeanFormDetails> forms = new ArrayList<BeanFormDetails>();
		Session session = factory.openSession();
		try {

			Criteria cr = session.createCriteria(BeanFormDetails.class);
			if(!status.equals("ALL")){
				cr.add(Restrictions.eq("status", status));
			}
			cr.add(Restrictions.in("formOwnerName", clientList));
			forms = new ArrayList<BeanFormDetails>(cr.list());
			return forms;

		} catch (Exception e) {
			e.printStackTrace();
			return forms;
		} finally {
			// close session
			session.close();
		}
	}

	@SuppressWarnings("unchecked")
	public static List<CollegeBean> getCollegeListOfBank(Integer bankId) {
		Session session = factory.openSession();
		List<CollegeBean> collList = null;
		try {
			collList = (List<CollegeBean>) session.createCriteria(CollegeBean.class)
					.add(Restrictions.eq("bankDetailsOTM.bankId", bankId)).list();
			log.info("size of the college is:" + collList.size());
		} catch (Exception e) {
			e.printStackTrace();
			return collList;
		} finally {
			session.close();
		}
		return collList;
	}

	public List<StateBean> getStateList() {
		Session session = factory.openSession();
		List<StateBean> StatesListBean = new ArrayList<StateBean>();
		try {
			StatesListBean = (List<StateBean>) session.createCriteria(StateBean.class).list();

		} catch (java.lang.NullPointerException ex) {
			return null;
		} finally {
			session.close();
		}

		return StatesListBean;
	}

	public List<ServicesBean> getServicesList() {
		Session session = factory.openSession();
		List<ServicesBean> ServicesListBean = new ArrayList<ServicesBean>();
		try {
			ServicesListBean = (List<ServicesBean>) session.createCriteria(ServicesBean.class).list();

		} catch (java.lang.NullPointerException ex) {
			return null;
		} finally {
			session.close();
		}

		return ServicesListBean;
	}

	public List<SuperAdminBean> getcompantList() {
		Session session = factory.openSession();
		List<SuperAdminBean> companyListBean = new ArrayList<SuperAdminBean>();
		try {
			companyListBean = (List<SuperAdminBean>) session.createCriteria(SuperAdminBean.class).list();

		} catch (java.lang.NullPointerException ex) {
			return null;
		} finally {
			session.close();
		}

		return companyListBean;
	}
	
	public static Integer getRowCount() {
		// Declarations

		// Open session from session factory
		Session session = factory.openSession();
		try {
			Criteria c = session.createCriteria(CollegeBean.class);
			c.addOrder(Order.desc("collegeId"));
			c.setMaxResults(1);
			CollegeBean temp = (CollegeBean) c.uniqueResult();
			return temp.getCollegeId() + 1;

		}catch(NullPointerException ex){
		
			return 0 + 1;
		
		}	
			finally {
		
			// close session
			session.close();
		}

	}

	public StateBean getStateBean(Integer stateId) {
		Session session = factory.openSession();
		StateBean StateBean = new StateBean();
		try {
			StateBean = (StateBean) session.get(StateBean.class, stateId);

		} catch (java.lang.NullPointerException ex) {
			return null;
		} finally {
			session.close();
		}

		return StateBean;
	}

	public ServicesBean getServicesBean(Integer serviceId) {
		Session session = factory.openSession();
		ServicesBean servicesBean = new ServicesBean();
		try {
			servicesBean = (ServicesBean) session.get(ServicesBean.class, serviceId);

		} catch (java.lang.NullPointerException ex) {
			return servicesBean;
		} finally {
			session.close();
		}

		return servicesBean;
	}

	public CompanyBean getCompany(Integer id) {
		Session session = factory.openSession();
		CompanyBean saBean = new CompanyBean();
		try {
			saBean = (CompanyBean) session.get(CompanyBean.class, id);

		} catch (java.lang.NullPointerException ex) {
			return saBean;
		} finally {
			session.close();
		}

		return saBean;
	}

	public CollegeBean registerClientDetailsDao(CollegeBean collegeBean) {
		Session session = factory.openSession();
		Transaction tx = session.beginTransaction();
		try {
			session.saveOrUpdate(collegeBean);
			tx.commit();
		}
		catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return collegeBean;

		} finally {
			session.close();
		}

		return collegeBean;
	}
		public String saveMappingCodeOfSabPaisaDetails(ClientMappingCodeOfSabPaisa mappingCodeOfSabPaisa) {
			Session session = factory.openSession();
			String value= "true";
			try {
				Transaction tx = session.beginTransaction();
				session.saveOrUpdate(mappingCodeOfSabPaisa);
				tx.commit();
			} catch (java.lang.NullPointerException ex) {

			}finally {
				session.close();
			}
	      return value;
		}

}

	
