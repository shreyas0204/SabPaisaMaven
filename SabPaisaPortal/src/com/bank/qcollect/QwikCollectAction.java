package com.bank.qcollect;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.net.URISyntaxException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.util.ArrayList;
import java.util.List;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.apache.struts2.ServletActionContext;

import com.communication.Mail;
import com.login.LoginAction;
import com.opensymphony.xwork2.ActionSupport;
import com.qcollect.beans.BankDetailsBean;
import com.qcollect.beans.BeanFormDetails;
import com.qcollect.beans.ClientMappingCodeOfSabPaisa;
import com.qcollect.beans.CollegeBean;
import com.qcollect.beans.CompanyBean;
import com.qcollect.beans.LoginBean;
import com.qcollect.beans.ServicesBean;
import com.qcollect.beans.StateBean;
import com.qcollect.beans.SuperAdminBean;

public class QwikCollectAction extends ActionSupport {
	HttpServletRequest request = ServletActionContext.getRequest();
	HttpServletResponse response = ServletActionContext.getResponse();
	HttpSession sesssion = ServletActionContext.getRequest().getSession();
	static Logger log = Logger.getLogger(QwikCollectAction.class.getName());
	private QwikCollectDAO qcDAO = new QwikCollectDAO();
	BankDetailsBean bankBean;
	List<CollegeBean> collegeBeanList = new ArrayList<>();
	private List<StateBean> stateList = new ArrayList<StateBean>();
	private List<ServicesBean> servicesList = new ArrayList<ServicesBean>();
	private List<SuperAdminBean> companyList = new ArrayList<SuperAdminBean>();
	CollegeBean collegeBean=new CollegeBean();
	private ClientMappingCodeOfSabPaisa mappingCodeOfSabPaisa = new ClientMappingCodeOfSabPaisa();
	
	
	private File userImage;
	private String userImageContentType;
	private String userImageFileName;

	private List<BeanFormDetails> forms = new ArrayList();

	public String GetAllForms() {
		List<String> clientList = new ArrayList<String>();
		try {
			bankBean = (BankDetailsBean) sesssion.getAttribute("sesBankDetailsBean");
			collegeBeanList = qcDAO.getCollegeListOfBank(bankBean.getBankId());

			for (int i = 0; i < collegeBeanList.size(); i++) {
				clientList.add(collegeBeanList.get(i).getCollegeCode());
			}

			forms = new ArrayList(qcDAO.getForms(clientList, "ALL"));
		} catch (NullPointerException ex) {
			log.info("exception in getting forms::" + ex);
			return "error";
		}
		return "success";
	}

	public String GetPendingForms() {
		List<String> clientList = new ArrayList<String>();
		try {
			bankBean = (BankDetailsBean) sesssion.getAttribute("sesBankDetailsBean");
			collegeBeanList = qcDAO.getCollegeListOfBank(bankBean.getBankId());

			for (int i = 0; i < collegeBeanList.size(); i++) {
				clientList.add(collegeBeanList.get(i).getCollegeCode());
			}

			forms = new ArrayList(qcDAO.getForms(clientList, "NEW"));
		} catch (NullPointerException ex) {
			log.info("exception in getting forms::" + ex);
			return "error";
		}
		return "success";
	}

	public String GetAllClients() {

		bankBean = (BankDetailsBean) sesssion.getAttribute("sesBankDetailsBean");
		collegeBeanList = qcDAO.getCollegeListOfBank(bankBean.getBankId());

		return SUCCESS;
	}

	public String addClient() {

		stateList = qcDAO.getStateList();
		servicesList = qcDAO.getServicesList();
		companyList = qcDAO.getcompantList();

		return SUCCESS;
	}

	public String saveClientDetailsAction() throws InvalidKeyException, NoSuchAlgorithmException,
			InvalidKeySpecException, InvalidAlgorithmParameterException, IllegalBlockSizeException, BadPaddingException,
			IOException, URISyntaxException {

		String filePath = ServletActionContext.getServletContext().getRealPath("/").concat("userimages");
		String ext = null;
		Integer totalFileSize = 0;
		try {
			bankBean = (BankDetailsBean) sesssion.getAttribute("sesBankDetailsBean");

		} catch (NullPointerException ex) {
			ex.printStackTrace();
			request.setAttribute("msg", "Please ");
			return "failure";
		}

		String username = new String();
		// generate credentials for admin login
		String password = RandomPasswordGenerator.generatePswd(6, 8, 1, 2, 0);
		try {
			username = "Ins".concat(collegeBean.getCollegeName()).replaceAll("\\s+", "").substring(0, 4)
					.concat(qcDAO.getRowCount().toString());

		} catch (java.lang.NullPointerException e) {

		}
		PasswordEncryption.encrypt(password);
		String encryptedPwd = PasswordEncryption.encStr;
		LoginBean creds = new LoginBean();
		creds.setPassword(encryptedPwd);
		creds.setUserName(username);
		creds.setProfile("Institute");
		creds.setCollgBean(collegeBean);

		try {

			ext = FilenameUtils.getExtension(this.userImageFileName);
			userImageFileName = StringUtils.removeEnd(userImageFileName, "." + ext);

			log.info("userImageFileName:" + userImageFileName);

			System.out.println("Server path:" + filePath);
			log.info("location of :" + filePath);
			File file = new File(filePath, this.userImageFileName);
			log.info("file Name:" + file + " : userImageContentType:" + userImageContentType);
			FileUtils.copyFile(this.userImage, file);
			byte[] bFile = new byte[(int) file.length()];
			try {
				FileInputStream fileInputStream = new FileInputStream(file);
				fileInputStream.read(bFile);
				fileInputStream.close();
			} catch (Exception e) {
				e.printStackTrace();
			}

			collegeBean.setCollegeImage(bFile);
			collegeBean.setCollegeLogo("img/" + userImageFileName);
			System.out.println("college image is:" + bFile);
		} catch (Exception ex) {
			ex.printStackTrace();
		}

		log.info("State Id is" + collegeBean.getStateBean().getStateId());
		log.info("Service Id is" + collegeBean.getServiceBean().getServiceId());
		log.info("ComId  is" + collegeBean.getCompanyBean().getId());
		StateBean stateBean = qcDAO.getStateBean(collegeBean.getStateBean().getStateId());
		ServicesBean servicesBean = qcDAO.getServicesBean(collegeBean.getServiceBean().getServiceId());
		CompanyBean compBean = qcDAO.getCompany(collegeBean.getCompanyBean().getId());
		log.info("Service Id is  33333");
		collegeBean.setLoginBean(creds);
		collegeBean.setBankDetailsOTM(bankBean);
		collegeBean.setStateBean(stateBean);
		collegeBean.setServiceBean(servicesBean);
		collegeBean.setCompanyBean(compBean);
		collegeBean = qcDAO.registerClientDetailsDao(collegeBean);
		if (collegeBean == null) {
			return ERROR;
		} else {
			
			mappingCodeOfSabPaisa.setBid(collegeBean.getBankDetailsOTM().getBankId().toString());
			mappingCodeOfSabPaisa.setCid(collegeBean.getCollegeId().toString());
			mappingCodeOfSabPaisa.setCMCode(collegeBean.getCollegeCode());
			mappingCodeOfSabPaisa.setCollegeBeanMappingToSabPaisaClient(collegeBean);

			String value = qcDAO.saveMappingCodeOfSabPaisaDetails(mappingCodeOfSabPaisa);

			String emailContent = "Welcome to the QwiCollect portal of " + collegeBean.getCollegeName();
			Mail email = new Mail();
			email.sendRegMail("Welcome To QwikCollect !", collegeBean.getEmailId(), username, password, emailContent,
					"");

			request.setAttribute("msg", "Client Added Successfully");
			
			return SUCCESS;
		}

	}

	public List<BeanFormDetails> getForms() {
		return forms;
	}

	public void setForms(List<BeanFormDetails> forms) {
		this.forms = forms;
	}

	public BankDetailsBean getBankBean() {
		return bankBean;
	}

	public void setBankBean(BankDetailsBean bankBean) {
		this.bankBean = bankBean;
	}

	public List<CollegeBean> getCollegeBeanList() {
		return collegeBeanList;
	}

	public void setCollegeBeanList(List<CollegeBean> collegeBeanList) {
		this.collegeBeanList = collegeBeanList;
	}

	public List<StateBean> getStateList() {
		return stateList;
	}

	public void setStateList(List<StateBean> stateList) {
		this.stateList = stateList;
	}

	public List<ServicesBean> getServicesList() {
		return servicesList;
	}

	public void setServicesList(List<ServicesBean> servicesList) {
		this.servicesList = servicesList;
	}

	public List<SuperAdminBean> getCompanyList() {
		return companyList;
	}

	public void setCompanyList(List<SuperAdminBean> companyList) {
		this.companyList = companyList;
	}

	public CollegeBean getCollegeBean() {
		return collegeBean;
	}

	public void setCollegeBean(CollegeBean collegeBean) {
		this.collegeBean = collegeBean;
	}

	public File getUserImage() {
		return userImage;
	}

	public void setUserImage(File userImage) {
		this.userImage = userImage;
	}

	public String getUserImageContentType() {
		return userImageContentType;
	}

	public void setUserImageContentType(String userImageContentType) {
		this.userImageContentType = userImageContentType;
	}

	public String getUserImageFileName() {
		return userImageFileName;
	}

	public void setUserImageFileName(String userImageFileName) {
		this.userImageFileName = userImageFileName;
	}
	

}
