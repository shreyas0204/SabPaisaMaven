package com.login;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;


public class LoginBean {
	private static final long serialVersionUID = 1L;
	
	private Integer loginId;

	private String userName;
	private String password;
	private String profile;
	private Integer referenceId;
	private Integer org_Id_Fk;
	private Integer userId_Fk;
	private Integer super_Admin_Id_Fk;

	public Integer getOrg_Id_Fk() {
		return org_Id_Fk;
	}

	public void setOrg_Id_Fk(Integer org_Id_Fk) {
		this.org_Id_Fk = org_Id_Fk;
	}

	public Integer getUserId_Fk() {
		return userId_Fk;
	}

	public void setUserId_Fk(Integer userId_Fk) {
		this.userId_Fk = userId_Fk;
	}

	public Integer getSuper_Admin_Id_Fk() {
		return super_Admin_Id_Fk;
	}

	public void setSuper_Admin_Id_Fk(Integer super_Admin_Id_Fk) {
		this.super_Admin_Id_Fk = super_Admin_Id_Fk;
	}

	public Integer getLoginId() {
		return loginId;
	}

	public void setLoginId(Integer loginId) {
		this.loginId = loginId;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getProfile() {
		return profile;
	}

	public void setProfile(String profile) {
		this.profile = profile;
	}

	public Integer getReferenceId() {
		return referenceId;
	}

	public void setReferenceId(Integer referenceId) {
		this.referenceId = referenceId;
	}

}
