package com.login;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import org.hibernate.SessionFactory;

import com.sabpaisaapp.portal.dbconnection.ConnectionClass;
import com.sabpaisaapp.portal.dbconnection.DBConnection;

public class LoginDAO {
	DBConnection connection = new DBConnection();
	Connection conn = null;
	Statement stmt = null;
	ResultSet rs = null;
	LoginBean loginBean=new LoginBean();
	public static SessionFactory factory = ConnectionClass.getSessionFactoryfactoryQcollect();

	public LoginBean getLoginDetails(String userName, String password) throws SQLException {
		conn = connection.getConnection();
		try {
			String sql = "SELECT * FROM sabpaisaportal.login_master where username='" + userName + "' and password='"
					+ password + "'";

			stmt= conn.createStatement();
			rs = stmt.executeQuery(sql);
		
			if (rs.next()) {
				loginBean.setLoginId(rs.getInt("loginId"));
				loginBean.setUserName(rs.getString("userName"));
				loginBean.setReferenceId(rs.getInt("refrenceId"));
				loginBean.setProfile(rs.getString("profile"));
				loginBean.setOrg_Id_Fk(rs.getInt("org_Id_Fk"));
				loginBean.setUserId_Fk(rs.getInt("userId_Fk"));
				loginBean.setSuper_Admin_Id_Fk(rs.getInt("super_Admin_Id_Fk"));

			} else {
				loginBean.setProfile("N/A");
			}

		} catch (Exception e) {
			e.printStackTrace();
			loginBean=null;
			
		}finally{conn.close();}

		return loginBean;
	}

}
