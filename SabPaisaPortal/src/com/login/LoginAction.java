package com.login;

import java.io.UnsupportedEncodingException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.apache.struts2.ServletActionContext;

import com.bank.qcollect.QwikCollectDAO;
import com.opensymphony.xwork2.ActionSupport;
import com.qcollect.beans.BankDetailsBean;
import com.sabpaisaapp.portal.enPasswordGenerator.PasswordEncryption;
import com.sabpaisaapp.portal.user.UserBean;
import com.sabpaisaapp.portal.user.UserDAO;

public class LoginAction extends ActionSupport {
	HttpServletRequest request = ServletActionContext.getRequest();
	HttpSession httpSession = request.getSession();
	HttpServletResponse response = ServletActionContext.getResponse();

	HttpSession sesssion = ServletActionContext.getRequest().getSession();

	static Logger log = Logger.getLogger(LoginAction.class.getName());
	private UserBean userDetailsBean = new UserBean();
	private BankDetailsBean bankDetailsBean = new BankDetailsBean();
    private PasswordEncryption passwordEnc=new PasswordEncryption();
	LoginBean loginBean = new LoginBean();
	LoginDAO loginDao = new LoginDAO();
	QwikCollectDAO qcDAO = new QwikCollectDAO();
	UserDAO userDao = new UserDAO();

	public String loginAction() throws InvalidKeyException, NoSuchAlgorithmException, InvalidKeySpecException, InvalidAlgorithmParameterException, UnsupportedEncodingException, IllegalBlockSizeException, BadPaddingException {

		String userName = request.getParameter("userName");
		String password = request.getParameter("password");
		String msg = null;
		PasswordEncryption.encrypt(password);

		String encryptedPwd = PasswordEncryption.encStr;
		System.out.println("In side the Login Action encryptdePed is :::::::::" +encryptedPwd);
		String profile = null;
		try {
			loginBean = loginDao.getLoginDetails(userName, encryptedPwd);
			if (loginBean != null) {

				profile = loginBean.getProfile();
System.out.println("Profile Return ::::::::::::::::::" +profile);
				if (profile.equals("BANK")) {
					log.info("Inside BANK" + loginBean.getReferenceId());
					bankDetailsBean = qcDAO.getBankDetails(loginBean.getReferenceId());
					sesssion.setAttribute("sesBankDetailsBean", bankDetailsBean);
				} else if (profile.equals("SA")) {

				} else if (profile.equals("ADMIN")) {
					sesssion.setAttribute("adminLogin", loginBean);

					userDetailsBean = userDao.getAdminDetails(loginBean.getLoginId());

				} else {
					msg = "Invalid User Name or Password";
					request.setAttribute("msg", msg);
					return ERROR;
				}

			} else {
				msg = "Invalid User Name or Password";
				request.setAttribute("msg", msg);
				return ERROR;
			}

		} catch (Exception e) {
			msg = "Invalid User Name or Password";
			request.setAttribute("msg", msg);
			return ERROR;
		}

		return profile;
	}

	public String logOut() {
		HttpSession newsession = request.getSession(false);
		if (newsession != null) {
			newsession.invalidate();

		}
		return SUCCESS;
	}

	public LoginBean getLoginBean() {
		return loginBean;
	}

	public void setLoginBean(LoginBean loginBean) {
		this.loginBean = loginBean;
	}

	public BankDetailsBean getBankDetailsBean() {
		return bankDetailsBean;
	}

	public void setBankDetailsBean(BankDetailsBean bankDetailsBean) {
		this.bankDetailsBean = bankDetailsBean;
	}

}
