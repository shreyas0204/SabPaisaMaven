package com.qcollect.beans;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

import com.qcollect.beans.CollegeBean;
import com.qcollect.beans.CompanyBean;

@Entity
@Table(name = "bank_details_bean")
public class BankDetailsBean implements Serializable {

	@GenericGenerator(name = "g1", strategy = "increment")
	@Id
	@GeneratedValue(generator = "g1")
	private Integer bankId;
	private String bankname, branch, contactPerson, emailId, bankLogo,address,bankLink;
	private Long contactNumber;
	 @Lob
	    @Column(name="bankImage",columnDefinition="mediumblob")
	    private byte[] bankImage;
	

	public byte[] getBankImage() {
		return bankImage;
	}

	public void setBankImage(byte[] bankImage) {
		this.bankImage = bankImage;
	}

	@ManyToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "comp_id_fk", referencedColumnName = "id")
	private CompanyBean companyBean;
	
	
	
	
	public CompanyBean getCompanyBean() {
		return companyBean;
	}

	public void setCompanyBean(CompanyBean companyBean) {
		this.companyBean = companyBean;
	}

	@OneToMany(cascade = CascadeType.ALL, mappedBy = "bankDetailsOTM")
	List<CollegeBean> listOfColleges = new ArrayList<CollegeBean>();

	public List<CollegeBean> getListOfColleges() {
		return listOfColleges;
	}

	public void setListOfColleges(List<CollegeBean> listOfColleges) {
		this.listOfColleges = listOfColleges;
	}

	public Integer getBankId() {
		return bankId;
	}

	public void setBankId(Integer bankId) {
		this.bankId = bankId;
	}

	
	public String getBankname() {
		return bankname;
	}

	public void setBankname(String bankname) {
		this.bankname = bankname;
	}
	public String getBranch() {
		return branch;
	}

	public void setBranch(String branch) {
		this.branch = branch;
	}
	public String getContactPerson() {
		return contactPerson;
	}

	public void setContactPerson(String contactPerson) {
		this.contactPerson = contactPerson;
	}
	public String getEmailId() {
		return emailId;
	}

	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}

	public Long getContactNumber() {
		return contactNumber;
	}

	public void setContactNumber(Long contactNumber) {
		this.contactNumber = contactNumber;
	}

	public String getBankLogo() {
		return bankLogo;
	}

	public void setBankLogo(String bankLogo) {
		this.bankLogo = bankLogo;
	}
	/*@Pattern(regexp = "^[0-9a-zA-Z_,/.'#@]*$", message = "Address has invalid characters")*/
	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getBankLink() {
		return bankLink;
	}

	public void setBankLink(String bankLink) {
		this.bankLink = bankLink;
	}
}
