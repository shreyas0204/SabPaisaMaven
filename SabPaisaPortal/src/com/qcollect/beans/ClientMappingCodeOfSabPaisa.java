package com.qcollect.beans;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;


import com.qcollect.beans.CollegeBean;


@Entity
@Table(name="client_mapping_code")
public class ClientMappingCodeOfSabPaisa implements Serializable {
	
	
	
	@GenericGenerator(name = "g1", strategy = "increment")
	@Id
	@GeneratedValue(generator = "g1")
	private Integer cMCId;
	private String  cMCode,cMProfile,cid,bid,clientUrl;
	public Integer getCMCId() {
		return cMCId;
	}
	
	public void setCMCId(Integer cMCId) {
		this.cMCId = cMCId;
	}
	
	/*@Pattern(regexp = "[0-9a-z-A-Z]*", message = "Name has invalid characters")*/
	public String getCMCode() {
		return cMCode;
	}
	
	public void setCMCode(String cMCode) {
		this.cMCode = cMCode;
	}
	
	/*@Pattern(regexp = "[0-9a-zA-Z]*", message = "Name has invalid characters")*/
	public String getCMProfile() {
		return cMProfile;
	}
	
	public void setCMProfile(String cMProfile) {
		this.cMProfile = cMProfile;
	}
	
	/*@URL(message = "Invalid website address")*/
	public String getClientUrl() {
		return clientUrl;
	}
	
	public void setClientUrl(String clientUrl) {
		this.clientUrl = clientUrl;
	}
	
	
	
	@ManyToOne(cascade=CascadeType.ALL)
	private CollegeBean collegeBeanMappingToSabPaisaClient;
	
	public CollegeBean getCollegeBeanMappingToSabPaisaClient() {
		return collegeBeanMappingToSabPaisaClient;
	}
	public void setCollegeBeanMappingToSabPaisaClient(CollegeBean collegeBeanMappingToSabPaisaClient) {
		this.collegeBeanMappingToSabPaisaClient = collegeBeanMappingToSabPaisaClient;
	}
	
	public String getCid() {
		return cid;
	}
	public void setCid(String cid) {
		this.cid = cid;
	}
	public String getBid() {
		return bid;
	}
	public void setBid(String bid) {
		this.bid = bid;
	}
	
	

}
