package com.qcollect.beans;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.annotations.GenericGenerator;

import com.qcollect.beans.CompanyBean;
import com.qcollect.beans.BeanFeeDetails;

@Entity
@Table(name = "data_form_details")
public class BeanFormDetails {

	@GenericGenerator(name = "g1", strategy = "increment")
	@Id
	@GeneratedValue(generator = "g1")
	private Integer id;

	

	@Column(unique = true)
	private String formName;
	private Date formDate, formStartDate, formEndDate, formLateEndDate;
	private Integer formOwnerId, validityflag;
	private String status, formOwnerName, saComment,image;
	private String jsEnabled,js_name,life_cycle,landingpage_srcPath,hasInstructions,fileLabel,file_ext,file_actu_name;
	private String isAadhaarVerified,isPANVerified,verificationFlag;
	@Lob
	@Column(name = "instructions", columnDefinition = "mediumblob")
	private byte[] instructions;
	@Transient
	private String fromDateStr, toDateStr, toLateDateStr;
	
	
	private Integer status_by, payer_type;
	
	private Integer target_actor;

	@OneToMany(cascade = CascadeType.ALL, orphanRemoval = true)
	private Set<BeanFormStructure> formStructure;

	@OneToOne(cascade = CascadeType.ALL, orphanRemoval = true)
	private BeanFeeDetails formFee;

	@Transient
	private List<BeanFormStructure> structureList;

	@OneToMany(cascade = CascadeType.ALL, orphanRemoval = true)
	private Set<FormLifeCycleBean> formcycles;

	
	@ManyToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "comp_id_fk", referencedColumnName = "id")
	private CompanyBean companyBean;
	
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getFormName() {
		return formName;
	}

	public void setFormName(String formName) {
		this.formName = formName;
	}

	public Date getFormDate() {
		return formDate;
	}

	public void setFormDate(Date formDate) {
		this.formDate = formDate;
	}

	public Integer getFormOwnerId() {
		return formOwnerId;
	}

	public void setFormOwnerId(Integer formOwnerId) {
		this.formOwnerId = formOwnerId;
	}

	public Set<BeanFormStructure> getFormStructure() {
		return formStructure;
	}

	public void setFormStructure(Set<BeanFormStructure> formStructure) {
		this.formStructure = formStructure;
	}

	public BeanFeeDetails getFormFee() {
		return formFee;
	}

	public void setFormFee(BeanFeeDetails formFee) {
		this.formFee = formFee;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Integer getStatus_by() {
		return status_by;
	}

	public void setStatus_by(Integer status_by) {
		this.status_by = status_by;
	}

	public Integer getPayer_type() {
		return payer_type;
	}

	public void setPayer_type(Integer payer_type) {
		this.payer_type = payer_type;
	}

	public Date getFormStartDate() {
		return formStartDate;
	}

	public void setFormStartDate(Date formStartDate) {
		this.formStartDate = formStartDate;
	}

	public Date getFormEndDate() {
		return formEndDate;
	}

	public void setFormEndDate(Date formEndDate) {
		this.formEndDate = formEndDate;
	}

	public String getFromDateStr() {

		return fromDateStr;
	}

	public void setFromDateStr(String fromDateStr) {
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		try {
			formStartDate = formatter.parse(fromDateStr);
		} catch (ParseException e) {
		}
		this.fromDateStr = fromDateStr;
	}

	public String getToDateStr() {
		return toDateStr;
	}

	public void setToDateStr(String toDateStr) {
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		try {
			formEndDate = formatter.parse(toDateStr);
		} catch (ParseException e) {
		}
		this.toDateStr = toDateStr;
	}

	public Date getFormLateEndDate() {
		return formLateEndDate;
	}

	public void setFormLateEndDate(Date formLateEndDate) {
		this.formLateEndDate = formLateEndDate;
	}

	public String getToLateDateStr() {
		return toLateDateStr;
	}

	public void setToLateDateStr(String toLateDateStr) {
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		try {
			formLateEndDate = formatter.parse(toLateDateStr);
		} catch (ParseException e) {
		}
		this.toLateDateStr = toLateDateStr;
	}

	public String getFormOwnerName() {
		return formOwnerName;
	}

	public void setFormOwnerName(String formOwnerName) {
		this.formOwnerName = formOwnerName;
	}

	public String getSaComment() {
		return saComment;
	}

	public void setSaComment(String saComment) {
		this.saComment = saComment;
	}

	public List<BeanFormStructure> getStructureList() {
		return structureList;
	}

	public void setStructureList(List<BeanFormStructure> structureList) {
		this.structureList = structureList;
	}

	public Integer getValidityflag() {
		return validityflag;
	}

	public void setValidityflag(Integer validityflag) {
		this.validityflag = validityflag;
	}

	public String getJsEnabled() {
		return jsEnabled;
	}

	public void setJsEnabled(String jsEnabled) {
		this.jsEnabled = jsEnabled;
	}

	public Set<FormLifeCycleBean> getFormcycles() {
		return formcycles;
	}

	public void setFormcycles(Set<FormLifeCycleBean> formcycles) {
		this.formcycles = formcycles;
	}

/*	public String getJs_name() {
		return js_name;
	}

	public void setJs_name(String js_name) {
		this.js_name = js_name;
	}*/

	public Integer getTarget_actor() {
		return target_actor;
	}

	public void setTarget_actor(Integer target_actor) {
		this.target_actor = target_actor;
	}

	public String getLife_cycle() {
		return life_cycle;
	}

	public void setLife_cycle(String life_cycle) {
		this.life_cycle = life_cycle;
	}

	public String getLandingpage_srcPath() {
		return landingpage_srcPath;
	}

	public void setLandingpage_srcPath(String landingpage_srcPath) {
		this.landingpage_srcPath = landingpage_srcPath;
	}

	public String getHasInstructions() {
		return hasInstructions;
	}

	public void setHasInstructions(String hasInstructions) {
		this.hasInstructions = hasInstructions;
	}

	public byte[] getInstructions() {
		return instructions;
	}

	public void setInstructions(byte[] instructions) {
		this.instructions = instructions;
	}

	public String getFileLabel() {
		return fileLabel;
	}

	public void setFileLabel(String fileLabel) {
		this.fileLabel = fileLabel;
	}

	public String getFile_ext() {
		return file_ext;
	}

	public void setFile_ext(String file_ext) {
		this.file_ext = file_ext;
	}

	public void setImage(String userImageFileName) {
		// TODO Auto-generated method stub
		
	}
	public String getImage() {
		return image;
	}
	public String getFile_actu_name() {
		return file_actu_name;
	}

	public void setFile_actu_name(String file_actu_name) {
		this.file_actu_name = file_actu_name;
	}

	public CompanyBean getCompanyBean() {
		return companyBean;
	}

	public void setCompanyBean(CompanyBean companyBean) {
		this.companyBean = companyBean;
	}

	public String getJs_name() {
		return js_name;
	}

	public void setJs_name(String js_name) {
		this.js_name = js_name;
	}

	public String getIsAadhaarVerified() {
		return isAadhaarVerified;
	}

	public void setIsAadhaarVerified(String isAadhaarVerified) {
		this.isAadhaarVerified = isAadhaarVerified;
	}

	public String getIsPANVerified() {
		return isPANVerified;
	}

	public void setIsPANVerified(String isPANVerified) {
		this.isPANVerified = isPANVerified;
	}

	public String getVerificationFlag() {
		return verificationFlag;
	}

	public void setVerificationFlag(String verificationFlag) {
		this.verificationFlag = verificationFlag;
	}



}
