package com.qcollect.beans;

import java.io.File;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.OrderBy;
import javax.persistence.Table;
import javax.servlet.http.HttpSession;


import org.hibernate.annotations.GenericGenerator;

import com.qcollect.beans.ActorBean;
import com.qcollect.beans.LoginBean;
import com.qcollect.beans.CompanyBean;
import com.qcollect.beans.SuperAdminBean;

import com.qcollect.beans.BankDetailsBean;
import com.qcollect.beans.ClientMappingCodeOfSabPaisa;
import com.qcollect.beans.PayeerTargetMappingToClient;
import com.qcollect.beans.SubInstitute;

@Entity
@Table(name = "college_master")
public class CollegeBean implements Serializable {

	@GenericGenerator(name = "g1", strategy = "increment")
	@Id
	@GeneratedValue(generator = "g1")
	private Integer collegeId;
	// required for receipt generation
	private String collegeCode;
	private Integer eGFDays;
	private String collegeName, contact, emailId, address, state, collegeLogo,collegeURL;
	private File collegeFile;

	@OneToMany(cascade = CascadeType.ALL, orphanRemoval = true)
	private Set<ActorBean> actors;

	@Lob
	@Column(name = "CollegeImage", columnDefinition = "mediumblob")
	private byte[] CollegeImage;

	public byte[] getCollegeImage() {
		return CollegeImage;
	}

	public void setCollegeImage(byte[] collegeImage) {
		CollegeImage = collegeImage;
	}

	@ManyToOne(cascade = CascadeType.ALL)
	private BankDetailsBean bankDetailsOTM;
	
	@ManyToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "stateId_Fk", referencedColumnName = "stateId")
	private StateBean stateBean;
	
	@ManyToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "serviceId_Fk", referencedColumnName = "serviceId")
	private ServicesBean serviceBean;
	
	
	@ManyToOne(cascade = CascadeType.PERSIST)
	@JoinColumn(name = "comp_id_fk", referencedColumnName = "id")
	private CompanyBean companyBean;
	
	
	
   public CompanyBean getCompanyBean() {
		return companyBean;
	}

	public void setCompanyBean(CompanyBean companyBean) {
		this.companyBean = companyBean;
	}

	/* @ManyToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "saId_Fk", referencedColumnName = "id")
	private SuperAdminBean superAdminBean;
*/
	@OneToOne(cascade = CascadeType.ALL, mappedBy = "collgBean")
	LoginBean loginBean;
	
	/*public SuperAdminBean getSuperAdminBean() {
		return superAdminBean;
	}

	public void setSuperAdminBean(SuperAdminBean superAdminBean) {
		this.superAdminBean = superAdminBean;
	}*/

	public List<SubInstitute> getListOfSubInstitute() {
		return listOfSubInstitute;
	}

	public StateBean getStateBean() {
		return stateBean;
	}

	public void setStateBean(StateBean stateBean) {
		this.stateBean = stateBean;
	}
	public ServicesBean getServiceBean() {
		return serviceBean;
	}



	public void setServiceBean(ServicesBean serviceBean) {
		this.serviceBean = serviceBean;
	}

	public void setListOfSubInstitute(List<SubInstitute> listOfSubInstitute) {
		this.listOfSubInstitute = listOfSubInstitute;
	}

	@OneToMany(cascade = CascadeType.ALL, mappedBy = "clientTargetMappingToCollegeBean")
	List<PayeerTargetMappingToClient> listOfPayeerTargetMappingToClient = new ArrayList<PayeerTargetMappingToClient>();

	public List<PayeerTargetMappingToClient> getListOfPayeerTargetMappingToClient() {
		return listOfPayeerTargetMappingToClient;
	}

	public void setListOfPayeerTargetMappingToClient(
			List<PayeerTargetMappingToClient> listOfPayeerTargetMappingToClient) {
		this.listOfPayeerTargetMappingToClient = listOfPayeerTargetMappingToClient;
	}

	@OneToMany(cascade = CascadeType.ALL, mappedBy = "headCollegeBean")
	List<SubInstitute> listOfSubInstitute = new ArrayList<SubInstitute>();

	@OneToMany(cascade = CascadeType.ALL, mappedBy = "collegeBeanMappingToSabPaisaClient")
	List<ClientMappingCodeOfSabPaisa> listOfColMappingSClient = new ArrayList<ClientMappingCodeOfSabPaisa>();

	public List<ClientMappingCodeOfSabPaisa> getListOfColMappingSClient() {
		return listOfColMappingSClient;
	}

	public void setListOfColMappingSClient(List<ClientMappingCodeOfSabPaisa> listOfColMappingSClient) {
		this.listOfColMappingSClient = listOfColMappingSClient;
	}


	public LoginBean getLoginBean() {
		return loginBean;
	}

	public void setLoginBean(LoginBean loginBean) {
		this.loginBean = loginBean;
	}

	//one college can have multiple transaction managing relationship for that
	@OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL, mappedBy = "collegeBean")
	@OrderBy("transDate DESC")
	Set<SampleTransBean> transactions;

	public Integer getCollegeId() {
		return collegeId;
	}

	public void setCollegeId(Integer collegeId) {
		this.collegeId = collegeId;
	}

	public String getCollegeCode() {
		return collegeCode;
	}

	public void setCollegeCode(String collegeCode) {
		this.collegeCode = collegeCode;
	}

	/*@Pattern(regexp = "[a-zA-Z][a-zA-Z ]*", message = "Name has invalid characters")*/
	public String getCollegeName() {
		return collegeName;
	}

	public void setCollegeName(String collegeName) {
		this.collegeName = collegeName;
	}

	public BankDetailsBean getBankDetailsOTM() {
		return bankDetailsOTM;
	}

	public void setBankDetailsOTM(BankDetailsBean bankDetailsOTM) {
		this.bankDetailsOTM = bankDetailsOTM;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public Set<SampleTransBean> getTransactions() {
		return transactions;
	}

	public void setTransactions(Set<SampleTransBean> transactions) {
		this.transactions = transactions;
	}

	public String getCollegeLogo() {
		return collegeLogo;
	}

	public void setCollegeLogo(String collegeLogo) {
		this.collegeLogo = collegeLogo;
	}

	public String getContact() {
		return contact;
	}

	public void setContact(String contact) {
		this.contact = contact;
	}

   public String getEmailId() {
		return emailId;
	}

	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}


    /*@Pattern(regexp = "^[0-9a-zA-Z_,/.'#@]*$", message = "Address has invalid characters")*/
	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public Integer geteGFDays() {
		return eGFDays;
	}

	public void seteGFDays(Integer eGFDays) {
		this.eGFDays = eGFDays;
	}

	public Set<ActorBean> getActors() {
		return actors;
	}

	public void setActors(Set<ActorBean> actors) {
		this.actors = actors;
	}

	public File getCollegeFile() {
		return collegeFile;
	}

	public void setCollegeFile(File collegeFile) {
		this.collegeFile = collegeFile;
	}

	public String getCollegeURL() {
		return collegeURL;
	}

	public void setCollegeURL(String collegeURL) {
		this.collegeURL = collegeURL;
	}

	

	



	
	

}
