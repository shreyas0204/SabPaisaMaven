package com.qcollect.beans;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

@Entity
@Table(name = "lookup_state")
public class StateBean implements Serializable {

	@GenericGenerator(name = "g1", strategy = "increment")
	@Id
	@GeneratedValue(generator = "g1")
	private Integer stateId;
	private String stateCode,stateName;
	
	@OneToMany(mappedBy = "stateBean")
	List<CollegeBean> collegeBeanList;
	
	public Integer getStateId() {
		return stateId;
	}
	public void setStateId(Integer stateId) {
		this.stateId = stateId;
	}
	public String getStateCode() {
		return stateCode;
	}
	public void setStateCode(String stateCode) {
		this.stateCode = stateCode;
	}
	public String getStateName() {
		return stateName;
	}
	public List<CollegeBean> getCollegeBeanList() {
		return collegeBeanList;
	}
	public void setCollegeBeanList(List<CollegeBean> collegeBeanList) {
		this.collegeBeanList = collegeBeanList;
	}
	public void setStateName(String stateName) {
		this.stateName = stateName;
	}
	


}

