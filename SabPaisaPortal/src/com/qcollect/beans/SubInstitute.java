package com.qcollect.beans;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

import com.qcollect.beans.CollegeBean;

@Entity
@Table(name="sub_institute_master")
public class SubInstitute implements Serializable {

	
	private static final long serialVersionUID = 1L;
	@GenericGenerator(name = "g1", strategy = "increment")
	@Id
	@GeneratedValue(generator = "g1")
	private Integer instituteId;
	
	private String instituteCode;
	private String instituteName,address;
	private String parentInstituteCode;
	private String instituteType;
	@ManyToOne(cascade=CascadeType.ALL)
	private CollegeBean headCollegeBean;
	
	
	
	public Integer getInstituteId() {
		return instituteId;
	}



	public void setInstituteId(Integer instituteId) {
		this.instituteId = instituteId;
	}



	public String getInstituteCode() {
		return instituteCode;
	}



	public void setInstituteCode(String instituteCode) {
		this.instituteCode = instituteCode;
	}



	public String getInstituteName() {
		return instituteName;
	}



	public void setInstituteName(String instituteName) {
		this.instituteName = instituteName;
	}



	public String getAddress() {
		return address;
	}



	public void setAddress(String address) {
		this.address = address;
	}



	public CollegeBean getHeadCollegeBean() {
		return headCollegeBean;
	}



	public void setHeadCollegeBean(CollegeBean headCollegeBean) {
		this.headCollegeBean = headCollegeBean;
	}



	public String getParentInstituteCode() {
		return parentInstituteCode;
	}



	public void setParentInstituteCode(String parentInstituteCode) {
		this.parentInstituteCode = parentInstituteCode;
	}



	public String getInstituteType() {
		return instituteType;
	}



	public void setInstituteType(String instituteType) {
		this.instituteType = instituteType;
	}



	
}
