package com.qcollect.beans;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.servlet.http.HttpSession;

import org.hibernate.annotations.GenericGenerator;

import com.qcollect.beans.CollegeBean;
import com.qcollect.beans.StateBean;

@Entity
@Table(name = "super_admin_master_details")
public class SuperAdminBean {
	@GenericGenerator(name = "g1", strategy = "increment")
	@Id
	@GeneratedValue(generator = "g1")
	private Integer id;

	private String name, contact, email;
	private String pass;
	private String userName;


	/*@OneToMany(mappedBy = "superAdminBean")
	List<CollegeBean> collegeBeanList;*/

	
	@ManyToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "companyId_Fk", referencedColumnName = "id")
	private CompanyBean companyBean;
	

	
	
	public CompanyBean getCompanyBean() {
		return companyBean;
	}

	public void setCompanyBean(CompanyBean companyBean) {
		this.companyBean = companyBean;
	}

	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}

	public String getContact() {
		return contact;
	}
	public void setContact(String contact) {
		this.contact = contact;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPass() {
		return pass;
	}
	
	public void setPass(String pass) {
		this.pass = pass;
	}

	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}

	/*public List<CollegeBean> getCollegeBeanList() {
		return collegeBeanList;
	}

	public void setCollegeBeanList(List<CollegeBean> collegeBeanList) {
		this.collegeBeanList = collegeBeanList;
	}*/

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}




}
