package com.qcollect.beans;

import java.util.Date;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Type;

import com.qcollect.beans.FormStateBean;
import com.qcollect.beans.BeanFormStructure;

@Entity
@Table(name = "data_form")
public class SampleFormBean {

	@GenericGenerator(name = "g1", strategy = "increment")
	@Id
	@GeneratedValue(generator = "g1")
	private Integer formId;
	@Type(type="text")
	private String formData;
	private Integer formTemplateId;
	@Column(unique = true)
	private String formTransId;
	private String formClientId;
	private Date formDate;
	private Integer formInstId, formApplicantId, formFeeId;
	private Double transAmount;
	private String name,contact,email,formFeeName,formStartDate,formEndDate,code, payerID, payerAadhaar, payerPAN;
	public String getPayerID() {
		return payerID;
	}

	public String getPayerAadhaar() {
		return payerAadhaar;
	}

	public void setPayerAadhaar(String payerAadhaar) {
		this.payerAadhaar = payerAadhaar;
	}

	public String getPayerPAN() {
		return payerPAN;
	}

	public void setPayerPAN(String payerPAN) {
		this.payerPAN = payerPAN;
	}

	public void setPayerID(String payerID) {
		this.payerID = payerID;
	}

	private String form_status;
	@Column(unique = true)
	private String formNumber;
	private String photo_ext,signature_ext;
	
	@OneToMany(cascade = CascadeType.ALL, orphanRemoval = true)
	private Set<FormFileBean>uploadedFiles;
	
	@Lob
	@Column(name = "photograph", columnDefinition = "mediumblob")
	private byte[] photograph;
	
	@Lob
	@Column(name = "signature", columnDefinition = "mediumblob")
	private byte[] signature;
	
	public String getFormNumber() {
		return formNumber;
	}

	public void setFormNumber(String formNumber) {
		this.formNumber = formNumber;
	}

	@OneToOne(cascade = CascadeType.ALL, orphanRemoval = true)
	private FormStateBean formstate;
	
	
	public String getForm_status() {
		return form_status;
	}

	public void setForm_status(String form_status) {
		this.form_status = form_status;
	}

	public FormStateBean getFormstate() {
		return formstate;
	}

	public void setFormstate(FormStateBean formstate) {
		this.formstate = formstate;
	}

	public String getFormClientId() {
		return formClientId;
	}

	public void setFormClientId(String formClientId) {
		this.formClientId = formClientId;
	}

	public String getFormFeeName() {
		return formFeeName;
	}

	public void setFormFeeName(String formFeeName) {
		this.formFeeName = formFeeName;
	}

	private Date dobDate;
	@Transient
	private String dob;
	public Integer getFormId() {
		return formId;
	}

	public void setFormId(Integer formId) {
		this.formId = formId;
	}

	public String getFormData() {
		return formData;
	}

	public void setFormData(String formData) {
		this.formData = formData;
	}

	public Integer getFormTemplateId() {
		return formTemplateId;
	}

	public void setFormTemplateId(Integer formTemplateId) {
		this.formTemplateId = formTemplateId;
	}

	

	public String getFormTransId() {
		return formTransId;
	}

	public void setFormTransId(String formTransId) {
		this.formTransId = formTransId;
	}

	public Date getFormDate() {
		return formDate;
	}

	public void setFormDate(Date formDate) {
		this.formDate = formDate;
	}

	public Integer getFormInstId() {
		return formInstId;
	}

	public void setFormInstId(Integer formInstId) {
		this.formInstId = formInstId;
	}

	public Integer getFormApplicantId() {
		return formApplicantId;
	}

	public void setFormApplicantId(Integer formApplicantId) {
		this.formApplicantId = formApplicantId;
	}

	public Integer getFormFeeId() {
		return formFeeId;
	}

	public void setFormFeeId(Integer formFeeId) {
		this.formFeeId = formFeeId;
	}

	public SampleFormBean(String formData, Integer formTemplateId,  Date formDate,
			Integer formInstId, Integer formApplicantId, Integer formFeeId,String name,Date dob,String email,String contact, Double amount ,String formFeeName,String startDate,String endDate,String code, String payerID, String appFormNumber) {
		super();
		this.formData = formData;
		this.formTemplateId = formTemplateId;
		this.formDate = formDate;
		this.formInstId = formInstId;
		this.formApplicantId = formApplicantId;
		this.formFeeId = formFeeId;
		this.name=name;
		this.dobDate=dob;
		this.contact=contact;
		this.email=email;
		this.transAmount=amount;
		this.formFeeName=formFeeName;
		this.formStartDate=startDate;
		this.formEndDate=endDate;
		this.code=code;
		this.payerID=payerID;		
		this.formNumber=appFormNumber;
	}

	public SampleFormBean() {
		// TODO Auto-generated constructor stub
	}

	

	public Double getTransAmount() {
		return transAmount;
	}

	public void setTransAmount(Double transAmount) {
		this.transAmount = transAmount;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getContact() {
		return contact;
	}

	public void setContact(String contact) {
		this.contact = contact;
	}

	public String getDob() {
		return dob;
	}

	public void setDob(String dob) {
		this.dob = dob;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Date getDobDate() {
		return dobDate;
	}

	public void setDobDate(Date dobDate) {
		this.dobDate = dobDate;
	}

	public String getFormStartDate() {
		return formStartDate;
	}

	public void setFormStartDate(String formStartDate) {
		this.formStartDate = formStartDate;
	}

	public String getFormEndDate() {
		return formEndDate;
	}

	public void setFormEndDate(String formEndDate) {
		this.formEndDate = formEndDate;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public byte[] getPhotograph() {
		return photograph;
	}

	public void setPhotograph(byte[] photograph) {
		this.photograph = photograph;
	}

	public byte[] getSignature() {
		return signature;
	}

	public void setSignature(byte[] signature) {
		this.signature = signature;
	}

	public Set<FormFileBean> getUploadedFiles() {
		return uploadedFiles;
	}

	public void setUploadedFiles(Set<FormFileBean> uploadedFiles) {
		this.uploadedFiles = uploadedFiles;
	}

	public String getPhoto_ext() {
		return photo_ext;
	}

	public void setPhoto_ext(String photo_ext) {
		this.photo_ext = photo_ext;
	}

	public String getSignature_ext() {
		return signature_ext;
	}

	public void setSignature_ext(String signature_ext) {
		this.signature_ext = signature_ext;
	}

	
	
	

}
