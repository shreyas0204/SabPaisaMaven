package com.qcollect.beans;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

import com.qcollect.beans.ActorBean;
import com.qcollect.beans.CollegeBean;
import com.qcollect.beans.SuperAdminBean;


@Entity
@Table(name = "login_master")
public class LoginBean implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@GenericGenerator(name = "g1", strategy = "increment")
	@Id
	@GeneratedValue(generator = "g1")
	private Integer loginId;
	private String userName;
	private String password;
	private String profile;

	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "collegeId_Fk", referencedColumnName = "collegeId")
	private CollegeBean collgBean;

	



public SuperAdminBean getSuperAdmin() {
		return superAdmin;
	}

	public void setSuperAdmin(SuperAdminBean superAdmin) {
		this.superAdmin = superAdmin;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	// one to one bidirectional relationship with super admin
	// child
	
	@OneToOne(cascade = CascadeType.ALL, targetEntity = SuperAdminBean.class)
	@JoinColumn(name = "id_Fk", referencedColumnName = "id")
	private SuperAdminBean superAdmin;
	
	

	@OneToOne(cascade = CascadeType.ALL, targetEntity = ActorBean.class)
	@JoinColumn(name = "actor_Id_Fk", referencedColumnName = "actor_id")
	private ActorBean actorBean;

	public CollegeBean getCollgBean() {
		return collgBean;
	}

	public void setCollgBean(CollegeBean collgBean) {
		this.collgBean = collgBean;
	}

	public String getProfile() {
		return profile;
	}

	public void setProfile(String profile) {
		this.profile = profile;
	}

	public Integer getLoginId() {
		return loginId;
	}

	public void setLoginId(Integer loginId) {
		this.loginId = loginId;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	/*
	 * public AffBean getAffBean() { return affBean; }
	 * 
	 * public void setAffBean(AffBean affBean) { this.affBean = affBean; }
	 * 
	 * public ParBean getParBean() { return parBean; }
	 * 
	 * public void setParBean(ParBean parBean) { this.parBean = parBean; }
	 */
	


	public ActorBean getActorBean() {
		return actorBean;
	}

	public void setActorBean(ActorBean actorBean) {
		this.actorBean = actorBean;
	}

}
