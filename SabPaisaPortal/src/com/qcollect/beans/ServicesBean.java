package com.qcollect.beans;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;


@Entity
@Table(name = "lookup_services")
public class ServicesBean {
	@GenericGenerator(name = "g1", strategy = "increment")
	@Id
	@GeneratedValue(generator = "g1")
	private Integer serviceId;
	private String serviceName;
	
	
	
	@OneToMany(mappedBy = "serviceBean")
	List<CollegeBean> collegeBeanList;
	
	public Integer getServiceId() {
		return serviceId;
	}
	public void setServiceId(Integer serviceId) {
		this.serviceId = serviceId;
	}
	public String getServiceName() {
		return serviceName;
	}
	public void setServiceName(String serviceName) {
		this.serviceName = serviceName;
	}
	public List<CollegeBean> getCollegeBeanList() {
		return collegeBeanList;
	}
	public void setCollegeBeanList(List<CollegeBean> collegeBeanList) {
		this.collegeBeanList = collegeBeanList;
	}
	
	

}
