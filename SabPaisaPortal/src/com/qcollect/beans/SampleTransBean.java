package com.qcollect.beans;

import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.GenericGenerator;

import com.qcollect.beans.CollegeBean;

@Entity  
@Table(name = "data_transactions")
public class SampleTransBean {

	@GenericGenerator(name = "g1", strategy = "increment")
	@Id
	@GeneratedValue(generator = "g1")
	private Integer id;
	private String transId, transStatus, transPaymode, transIdExt;
	@Temporal(TemporalType.TIMESTAMP)
	private Date transDate, txncloseDate;
	private Double transAmount, transOgAmount, transOtherChrg, actAmount;
	private String name, email, contact, bid, cid, refundJob, challanNo;
	private Date dob;
	private String issuerRefNo,  pgTransId, spRespCode, pgRespCode, payerID, feeName, transCharges;
	private Integer formId;

	// bidirectional relation with transaction bean
	@ManyToOne(targetEntity = CollegeBean.class)
	@JoinColumn(name = "college_Id_fk", referencedColumnName = "collegeId")
	private CollegeBean collegeBean;

	@OneToOne(cascade = CascadeType.ALL, orphanRemoval = true)
	private SampleFormBean transForm;
	
	@Column(unique = true)
	private String spTransId;
	
	@javax.persistence.Transient
	private String transCreationErrorFlag;

	public Integer getId() {
		return id;
	}

	public String getTransCreationErrorFlag() {
		return transCreationErrorFlag;
	}

	public void setTransCreationErrorFlag(String transCreationErrorFlag) {
		this.transCreationErrorFlag = transCreationErrorFlag;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getTransId() {
		return transId;
	}

	public void setTransId(String transId) {
		this.transId = transId;
	}

	public String getTransStatus() {
		return transStatus;
	}

	public void setTransStatus(String transStatus) {
		this.transStatus = transStatus;
	}

	public String getTransPaymode() {
		return transPaymode;
	}

	public void setTransPaymode(String transPaymode) {
		this.transPaymode = transPaymode;
	}

	public Date getTransDate() {
		return transDate;
	}

	public void setTransDate(Date transDate) {
		this.transDate = transDate;
	}

	public SampleFormBean getTransForm() {
		return transForm;
	}

	public void setTransForm(SampleFormBean transForm) {
		this.transForm = transForm;
	}

	public String getTransIdExt() {
		return transIdExt;
	}

	public void setTransIdExt(String transIdExt) {
		this.transIdExt = transIdExt;
	}

	public Double getTransAmount() {
		return transAmount;
	}

	public void setTransAmount(Double transAmount) {
		this.transAmount = transAmount;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getContact() {
		return contact;
	}

	public void setContact(String contact) {
		this.contact = contact;
	}

	public Date getDob() {
		return dob;
	}

	public void setDob(Date dob) {
		this.dob = dob;
	}

	public String getIssuerRefNo() {
		return issuerRefNo;
	}

	public void setIssuerRefNo(String issuerRefNo) {
		this.issuerRefNo = issuerRefNo;
	}

	public String getSpTransId() {
		return spTransId;
	}

	public void setSpTransId(String spTransId) {
		this.spTransId = spTransId;
	}

	public String getPgTransId() {
		return pgTransId;
	}

	public void setPgTransId(String pgTransId) {
		this.pgTransId = pgTransId;
	}

	public String getSpRespCode() {
		return spRespCode;
	}

	public void setSpRespCode(String spRespCode) {
		this.spRespCode = spRespCode;
	}

	public String getPgRespCode() {
		return pgRespCode;
	}

	public void setPgRespCode(String pgRespCode) {
		this.pgRespCode = pgRespCode;
	}

	public CollegeBean getCollegeBean() {
		return collegeBean;
	}

	public void setCollegeBean(CollegeBean collegeBean) {
		this.collegeBean = collegeBean;
	}

	public String getPayerID() {
		return payerID;
	}

	public void setPayerID(String payerID) {
		this.payerID = payerID;
	}

	public String getFeeName() {
		return feeName;
	}

	public void setFeeName(String feeName) {
		this.feeName = feeName;
	}

	public String getBid() {
		return bid;
	}

	public void setBid(String bid) {
		this.bid = bid;
	}

	public String getCid() {
		return cid;
	}

	public void setCid(String cid) {
		this.cid = cid;
	}

	public String getTransCharges() {
		return transCharges;
	}

	public void setTransCharges(String transCharges) {
		this.transCharges = transCharges;
	}

	public Double getTransOgAmount() {
		return transOgAmount;
	}

	public void setTransOgAmount(Double transOgAmount) {
		this.transOgAmount = transOgAmount;
	}

	public Double getTransOtherChrg() {
		return transOtherChrg;
	}

	public void setTransOtherChrg(Double transOtherChrg) {
		this.transOtherChrg = transOtherChrg;
	}

	public Double getActAmount() {
		return actAmount;
	}

	public void setActAmount(Double actAmount) {
		this.actAmount = actAmount;
	}

	public String getRefundJob() {
		return refundJob;
	}

	public void setRefundJob(String refundJob) {
		this.refundJob = refundJob;
	}

	public Date getTxncloseDate() {
		return txncloseDate;
	}

	public void setTxncloseDate(Date txncloseDate) {
		this.txncloseDate = txncloseDate;
	}

	public String getChallanNo() {
		return challanNo;
	}

	public void setChallanNo(String challanNo) {
		this.challanNo = challanNo;
	}

	public Integer getFormId() {
		return formId;
	}

	public void setFormId(Integer formId) {
		this.formId = formId;
	}
	

}
