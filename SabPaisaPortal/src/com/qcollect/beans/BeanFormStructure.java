package com.qcollect.beans;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Type;


@Entity
@Table(name = "data_form_structure")
public class BeanFormStructure implements Comparable<BeanFormStructure>{

	@GenericGenerator(name = "g1", strategy = "increment")
	@Id
	@GeneratedValue(generator = "g1")
	private Integer id;
	private Integer isMandatory;
	@Type(type="text")
	private String fieldValues;
	private Integer fieldOrder;
	private String jsEnabled;
	private String triggerEvent;
	private String jsFunction;
	
	@OneToOne(cascade = CascadeType.ALL,orphanRemoval=true, fetch = FetchType.EAGER)
	private BeanFieldLookup formField;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getIsMandatory() {
		return isMandatory;
	}

	public void setIsMandatory(Integer isMandatory) {
		this.isMandatory = isMandatory;
	}

	public String getFieldValues() {
		return fieldValues;
	}

	public void setFieldValues(String fieldValues) {
		this.fieldValues = fieldValues;
	}

	public BeanFieldLookup getFormField() {
		return formField;
	}

	public void setFormField(BeanFieldLookup formField) {
		this.formField = formField;
	}

	public Integer getFieldOrder() {
		return fieldOrder;
	}

	public void setFieldOrder(Integer fieldOrder) {
		this.fieldOrder = fieldOrder;
	}
	
	
	
	public String getJsEnabled() {
		return jsEnabled;
	}

	public void setJsEnabled(String jsEnabled) {
		this.jsEnabled = jsEnabled;
	}

	public String getTriggerEvent() {
		return triggerEvent;
	}

	public void setTriggerEvent(String triggerEvent) {
		this.triggerEvent = triggerEvent;
	}

	public String getJsFunction() {
		return jsFunction;
	}

	public void setJsFunction(String jsFunction) {
		this.jsFunction = jsFunction;
	}

	@Override
	public int compareTo(BeanFormStructure o) {
		// TODO Auto-generated method stub
	        int compareorder=o.getFieldOrder();
	        /* For Ascending order*/
	        return this.fieldOrder-compareorder;
	}
}
