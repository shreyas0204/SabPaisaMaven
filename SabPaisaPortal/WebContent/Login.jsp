<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>SabPaisa</title>
<link href="login/css/bootstrap.min.css" rel="stylesheet">
<link href="login/css/font-awesome.min.css" rel="stylesheet">
<link href="login/css/socialmedia.css" rel="stylesheet">
<link href="login/css/prettyPhoto.css" rel="stylesheet">
<link href="login/css/animate.css" rel="stylesheet">
<link href="login/css/main.css" rel="stylesheet">
<link href="login/css/style-tabs.css" rel="stylesheet">

<!--[if lt IE 9]>
    <script src="js/html5shiv.js"></script>
    <script src="js/respond.min.js"></script>
    <![endif]-->

<link rel="shortcut icon" href="login/images/ico/favicons.ico">
<link rel="apple-touch-icon-precomposed" sizes="144x144"
	href="login/images/ico/apple-touch-icon-144-precomposed.png">
<link rel="apple-touch-icon-precomposed" sizes="114x114"
	href="login/images/ico/apple-touch-icon-114-precomposed.png">
<link rel="apple-touch-icon-precomposed" sizes="72x72"
	href="login/images/ico/apple-touch-icon-72-precomposed.png">
<link rel="apple-touch-icon-precomposed"
	href="images/ico/apple-touch-icon-57-precomposed.png">
</head>
<!--/head-->
<body>
	<header class="navbar navbar-inverse navbar-fixed-top wet-asphalt"
		role="banner">
	<div class="container">
		<div class="navbar-header">
			<button type="button" class="navbar-toggle" data-toggle="collapse"
				data-target=".navbar-collapse">
				<span class="sr-only">Toggle navigation</span> <span
					class="icon-bar"></span> <span class="icon-bar"></span> <span
					class="icon-bar"></span>
			</button>
			<a class="navbar-brand" href="index.html"><img
				src="login/images/logo.png" alt="logo"></a>
		</div>
	</div>
	</header>
	<!--/header-->
	<section id="main-slider" class="no-margin">
	<div class="row">
		<div
			class="col-lg-5 col-md-12 col-sm-12 col-xs-12 topform-section rght-pad">
			<div class="col-sm-12">
				<div class="intro-text">
					<ul class="accordion-tabs-minimal">
						<li class="tab-header-and-content"><a href="#"
							class="tab-link is-active"> <!--<span class="pay"></span>-->Login
						</a>
							<div class="tab-content">

								<form action="loginProfile" method="post">
									<span class="main_header"> 
						  </span>
									<div class="col-sm-12 labeling">
										<div class="col-sm-12">
											<input class="form-control" name="userName" type="UserID"
												placeholder="User ID" id="">
										</div>
									</div>
									<div class="col-sm-12 labeling">

										<div class="col-sm-12">
											<input class="form-control" type="password" name="password"
												placeholder="Password" id="">
										</div>
									</div>

									<div class="col-md-12 cntrs labeling">
										<div class="col-sm-5"></div>
										<div class="col-sm-12">
											<button type="submit" class="btn-name">Proceed</button>
										</div>
									</div>
								</form>
							</div></li>


					</ul>
				</div>
			</div>
		</div>
	</div>

	<div id="my-carousel" class="carousel slide wet-asphalt"
		data-ride="carousel">
		<div class="carousel-inner">
			<div class="item active"
				style="background-image: url(login/images/default.jpg)">
				<!--<div class="container">
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="carousel-content centered">
                                    <h2 class="boxed animation animated-item-1">Powerful and Responsive Web Design</h2>
                                    <p class="boxed animation animated-item-2">Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas.</p>
                                </div>
                            </div>
                        </div>
                    </div>-->
			</div>
			<!--/.item-->


		</div>
		<!--/.carousel-inner-->
	</div>
	<!--/.carousel--> </section>
	<!--/#main-slider-->
	<section id="testimonial" class="alizarin-nth">
	<div class="container">

		<div class="row ">

			<div class="col-md-4 col-sm-6 sec-color">
				<div class="media">
					<div class="pull-left">
						<!--<i class="icon-windows icon-md"></i>-->
						<span class="icon-windows- icon-md-wht"><img
							src="login/images/stepfirst.png"></span>
					</div>
					<div class="media-body">
						<h3 class="media-heading">QwikCollect</h3>
						<p>Powerful dymanic form buidler for setting collection
							platform in few mins</p>
					</div>
				</div>
			</div>
			<!--/.col-md-4-->

			<div class="col-md-4 col-sm-6 sec-color">
				<div class="media">
					<div class="pull-left">
						<!--<i class="icon-android icon-md"></i>-->
						<span class="icon-windows- icon-md-wht"><img
							src="login/images/stepsecond.png"></span>
					</div>
					<div class="media-body">
						<h3 class="media-heading">SabPaisa</h3>
						<p>Payment Gateway with Online and Offline Mode.</p>
					</div>
				</div>
			</div>
			<!--/.col-md-4-->

			<div class="col-md-4 col-sm-6 sec-color">
				<div class="media">
					<div class="pull-left">
						<!--<i class="icon-apple icon-md"></i>-->
						<span class="icon-windows- icon-md-wht"><img
							src="login/images/stepthird.png"></span>
					</div>
					<div class="media-body">
						<h3 class="media-heading">Settlement</h3>
						<p>Settle millions of transaction in one click.</p>
					</div>
				</div>
			</div>
			<!--/.col-md-4-->

		</div>

	</div>
	</section>
	<!--/#testimonial-->


	<br />
	<br />
	<footer id="footer" class="midnight-blue"
		style="background:#ccc; color:#333;">
	<div class="container">
		<div class="row center">
			<div class="col-sm-12">
				&copy; 2017 <a target="_blank" href="http://srslive.in/"
					title="INDIA’S FIRST AND ONLY INTEGRATED PLATFORM FOR BOTH ONLINE AND OFFLINE PAYMENT/COLLECTIONS"
					alt="INDIA’S FIRST AND ONLY INTEGRATED PLATFORM FOR BOTH ONLINE AND OFFLINE PAYMENT/COLLECTIONS"
					style="color: #333;">Srs Live Technologies</a>. All Rights
				Reserved.
			</div>
			<!--<div class="gototop-li"><a id="gotlotop" class="gototop" href="#"><i class="icon-chevron-up"></i></a></div>-->
			<div id="back-top">
				<a href="#top"><span class="icon-chevron-up"></span></a>
			</div>
		</div>
	</div>
	</footer>
	<!--/#footer-->

	<script src="login/js/jquery.js"></script>
	<script src="login/js/bootstrap.min.js"></script>
	<script src="login/js/jquery.prettyPhoto.js"></script>
	<script src="login/js/jquery.isotope.min.js"></script>
	<script src="login/js/main.js"></script>
	<script src="login/js/index.js"></script>
	<script src="login/js/jquery.min.js"></script>


	<script>
	$(document).ready(function(){

	// hide #back-top first
	$("#back-top").hide();
	
	// fade in #back-top
	$(function () {
		$(window).scroll(function () {
			if ($(this).scrollTop() > 100) {
				$('#back-top').fadeIn();
			} else {
				$('#back-top').fadeOut();
			}
		});

		// scroll body to 0px on click
		$('#back-top a').click(function () {
			$('body,html').animate({
				scrollTop: 0
			}, 800);
			return false;
		});
	});

});
</script>
	<script>
		$(function(){
		$('.nav a').click(function(){
			$(this).parent().addClass('active').siblings().removeClass('active')	
		})
	})
	</script>
</body>
</html>