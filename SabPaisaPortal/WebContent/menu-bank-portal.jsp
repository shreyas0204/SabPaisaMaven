<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
	<%@page import="com.qcollect.beans.BankDetailsBean"%>
	<%@page import="java.util.*"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<body>

	<%
		BankDetailsBean bankBean = (BankDetailsBean) session.getAttribute("sesBankDetailsBean");
	%>
	<div class="navbar nav_title" style="border: 0;">
		<a href="index.html" class="site_title"><img
			src="data:image/jpeg;base64,<%=Base64.getEncoder().encodeToString(bankBean.getBankImage())%>"
			alt="..." class="img-circle profile_img"
			style="width: 50px; height: 50px; margin: 0;"><span
			class="col-title"><%=bankBean.getBankname()%></span></a>
	</div>

	<div class="clearfix"></div>

	<br />

	<div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
		<div class="menu_section">
			<h3>General</h3>
			<ul class="nav side-menu">
				<li><a href="bank-home.jsp">Home</a></li>
				<li><a href="bank-stactics.jsp"> Statics</a></li>
				<li><a href="getPendingForms"> Pending Forms</a></li>
				<li><a href="getAllForms"> All Forms</a></li>
				<li><a href="#">Client<span class="fa fa-chevron-down"></span></a>
					<ul class="nav child_menu">
						<li><a href="getAllClients">Client Details</a></li>
						<li><a href="addClient">Add New Client</a></li>
					</ul></li>
				<!-- <li><a href="#">Super Admin Details<span
						class="fa fa-chevron-down"></span></a>
					<ul class="nav child_menu">
						<li><a href="superadmin.html">SuperAdmin Details</a></li>
						<li><a href="addsuperadmin.html">Add New SuperAdmin</a></li>
					</ul></li> -->
			</ul>
		</div>

	</div>
	<!-- /sidebar menu -->

	<!-- /menu footer buttons -->
	<div class="sidebar-footer hidden-small">
		<img src="images/SabPaisa-logo.png" />
	</div>
	<!-- /menu footer buttons -->
	

</body>
</html>