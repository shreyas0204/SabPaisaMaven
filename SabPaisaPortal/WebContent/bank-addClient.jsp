<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Add Client</title>
<link rel="shortcut icon" href="images/ico/favicons.ico">
<!-- Bootstrap -->
<link href="vendors/bootstrap/dist/css/bootstrap.min.css"
	rel="stylesheet">
<!-- Font Awesome -->
<link href="vendors/font-awesome/css/font-awesome.min.css"
	rel="stylesheet">
<!-- NProgress -->
<link href="vendors/nprogress/nprogress.css" rel="stylesheet">


<!-- Custom Theme Style -->
<link href="build/css/customnew.css" rel="stylesheet">
<link href="build/css/custom.min.css" rel="stylesheet">
</head>

<body class="nav-md">

	<div class="container body">
		<div class="main_container">
			<div class="col-md-3 left_col">
				<div class="left_col scroll-view">
					<jsp:include page="menu-bank-portal.jsp"></jsp:include>
					<!-- /menu footer buttons -->
				</div>
			</div>

			<!-- top navigation -->
			<div class="top_nav">
				<div class="nav_menu">
					<nav>
					<div class="nav toggle">
						<a id="menu_toggle"><i class="fa fa-bars"></i></a>
					</div>
					<div class="welcomeback">Dashboard</div>
					<div class="welcomeback-img">
						<img src="images/Sab-Paisa-small.png">
					</div>
					<ul class="nav navbar-nav navbar-right">

						<li class=""><a href="javascript:;"
							class="user-profile dropdown-toggle" data-toggle="dropdown"
							aria-expanded="false"> <!--<img src="images/college-logo.png" alt="">-->Welcome
								<span class=" fa fa-angle-down"></span>
						</a>
							<ul class="dropdown-menu dropdown-usermenu pull-right">
								<li><a href="login/login.html"><i
										class="fa fa-sign-out pull-right"></i> Log Out</a></li>
							</ul></li>
					</ul>
					</nav>
				</div>
			</div>
			<!-- /top navigation -->

			<!-- page content -->
			<div class="right_col" role="main">
				<div class="">

					<div class="clearfix"></div>

					<div class="" role="tabpanel" data-example-id="togglable-tabs">
						<ul id="myTab" class="nav nav-tabs bar_tabs" role="tablist">
							<li role="presentation" class="active"><a
								href="#tab_content1" id="home-tab" role="tab" data-toggle="tab"
								aria-expanded="true"> Fill Client Details</a></li>
						</ul>
						<div id="myTabContent" class="tab-content">
							<div role="tabpanel" class="tab-pane fade active in"
								id="tab_content1" aria-labelledby="home-tab">
								<div class="row">
									<div class="col-md-12 col-sm-12 col-xs-12">
										<div class="x_panel">
											<div class="x_title">
												<h2>Fill Client Details</h2>
												<!--<a type="button" class="view" data-toggle="modal" data-target=".bs-example-modal-geninfo" style="float:right; padding:5px 0 0 0;">Edit</a>-->
												<div class="clearfix"></div>
											</div>
											<div class="x_content">
												<br />
												<form id="demo-form2" data-parsley-validate
													class="form-horizontal form-label-left" action="saveClient"
													method="POST" enctype="multipart/form-data">
													<input id="comId" name="collegeBean.companyBean.id"
														type="hidden" value="1">
													<div class="form-group">
														<label
															class="control-label col-md-3 col-sm-3 col-xs-12 txtfl"
															for="feed-description">Services <span
															class="required">*</span>
														</label>
														<div class="col-md-6 col-sm-6 col-xs-12">
															<select class="select2_single form-control" tabindex="-1"
																name="collegeBean.serviceBean.serviceId"
																required="required">
																<option selected="selected" disabled="disabled">Select
																	An Option</option>

																<c:forEach var="list" items="${servicesList}">
																	<option value="${list.serviceId}">
																		<c:out value="${list.serviceName}" /></option>
																</c:forEach>
															</select>
														</div>
													</div>
													<div class="form-group">
														<label
															class="control-label col-md-3 col-sm-3 col-xs-12 txtfl"
															for="feed-description">Client Name <span
															class="required">*</span>
														</label>
														<div class="col-md-6 col-sm-6 col-xs-12">
															<input type="text" id="feed-name" required="required"
																class="form-control col-md-7 col-xs-12"
																placeholder="Client Name" name="collegeBean.collegeName">
														</div>
													</div>
													<div class="form-group">
														<label
															class="control-label col-md-3 col-sm-3 col-xs-12 txtfl"
															for="feed-description">Client Code <span
															class="required">*</span>
														</label>
														<div class="col-md-6 col-sm-6 col-xs-12">
															<input type="text" id="feed-name" required="required"
																class="form-control col-md-7 col-xs-12"
																name="collegeBean.collegeCode" placeholder="Client Code">
														</div>
													</div>
													<div class="form-group">
														<label
															class="control-label col-md-3 col-sm-3 col-xs-12 txtfl"
															for="feed-description">Contact Number <span
															class="required">*</span>
														</label>
														<div class="col-md-6 col-sm-6 col-xs-12">
															<input type="number" id="feed-name" required="required"
																name="collegeBean.contact"
																class="form-control col-md-7 col-xs-12"
																pattern="[0-9]{10}" placeholder="Contact Number">
														</div>
													</div>
													<div class="form-group">
														<label
															class="control-label col-md-3 col-sm-3 col-xs-12 txtfl"
															for="feed-description">Email ID <span
															class="required">*</span>
														</label>
														<div class="col-md-6 col-sm-6 col-xs-12">
															<input type="email" id="feed-name" required="required"
																class="form-control col-md-7 col-xs-12"
																placeholder="Emaili ID" name="collegeBean.emailId">
														</div>
													</div>

													<div class="form-group">
														<label
															class="control-label col-md-3 col-sm-3 col-xs-12 txtfl"
															for="feed-name">State <span class="required">*</span>
														</label>
														<div class="col-md-6 col-sm-6 col-xs-12">

															<select class="select2_single form-control" tabindex="-1"
																name="collegeBean.stateBean.stateId">
																<option selected="selected" disabled="disabled">Select
																	An Option</option>
																<c:forEach var="list" items="${stateList}">
																	<option value="${list.stateId}">
																		<c:out value="${list.stateName}" /></option>

																</c:forEach>

															</select>

														</div>
													</div>
													<div class="form-group">
														<label
															class="control-label col-md-3 col-sm-3 col-xs-12 txtfl"
															for="feed-name">Logo <span class="required">*</span>
														</label>
														<div class="col-md-6 col-sm-6 col-xs-12">
															<input id="payer_type" name="userImage"
																placeholder="logo Name" type="file" required="required"
																title="Image should be of jpeg,gif,png,jpg format only"
																class="form-control"
																accept="image/gif, image/jpeg, image/jpg, image/png">

														</div>
													</div>
													<div class="form-group">
														<label
															class="control-label col-md-3 col-sm-3 col-xs-12 txtfl"
															for="feed-name">Address <span class="required">*</span>
														</label>
														<div class="col-md-6 col-sm-6 col-xs-12">
															<textarea rows="3" placeholder="Enter Your Address"
																class="form-control col-md-7 col-xs-12"
																name="collegeBean.address"></textarea>
														</div>
													</div>
													<div class="form-group">
														<label
															class="control-label col-md-3 col-sm-3 col-xs-12 txtfl"
															for="feed-description">URL <span class="required">*</span>
														</label>
														<div class="col-md-6 col-sm-6 col-xs-12">
															<input type="email" id="feed-name" required="required"
																class="form-control col-md-7 col-xs-12"
																placeholder="URL" name="collegeBean.collegeURL">
														</div>
													</div>



													<div class="ln_solid"></div>
													<div class="form-group">
														<div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
														
															<button type="submit" class="btn btn-primary">Save</button>
														</div>
													</div>
													

												</form>
											</div>

										</div>
									</div>
								</div>
							</div>


						</div>
					</div>




				</div>
			</div>

			<!-- /page content -->

			<!-- footer content -->
			<footer>
			<div class="pull-rights">
				<p>For any Queries, contact us on: 011-41733223, E-mail:
					qwik.collect@srslive.in</p>
				<ul>
					<li><a
						href="https://www.qwikcollect.in/QwikCollect/ContactUs.html"
						target="_blank">Contact Us</a></li>
					<li>|</li>
					<li><a
						href="https://www.qwikcollect.in/QwikCollect/PrivacyPolicy.html"
						target="_blank">Privacy Policy</a></li>
					<li>|</li>
					<li><a
						href="http://43.252.89.79/QwikCollect/TermsAndConditions.html"
						target="_blank">Terms & Conditions</a></li>
					<li>|</li>
					<li><a
						href="https://www.qwikcollect.in/QwikCollect/Disclaimer.html"
						target="_blank">Disclaimer</a></li>
				</ul>
				� COPYRIGHT 2017. Powered by SRS Live Technologies Pvt Ltd. <a
					href="http://www.sabpaisa.in/">SabPaisa</a>
			</div>
			<div class="clearfix"></div>
			</footer>
			<!-- /footer content -->
		</div>
	</div>

	<!-- jQuery -->
	<script src="vendors/jquery/dist/jquery.min.js"></script>
	<!-- Bootstrap -->
	<script src="vendors/bootstrap/dist/js/bootstrap.min.js"></script>
	<!-- FastClick -->
	<script src="vendors/fastclick/lib/fastclick.js"></script>
	<!-- NProgress -->
	<script src="vendors/nprogress/nprogress.js"></script>
	<!-- Chart.js -->
	<script src="vendors/Chart.js/dist/Chart.min.js"></script>
	<!-- jQuery Sparklines -->
	<script src="vendors/jquery-sparkline/dist/jquery.sparkline.min.js"></script>

	<!-- Flot -->
	<script src="vendors/Flot/jquery.flot.js"></script>
	<script src="vendors/Flot/jquery.flot.pie.js"></script>
	<script src="vendors/Flot/jquery.flot.time.js"></script>
	<script src="vendors/Flot/jquery.flot.stack.js"></script>
	<script src="vendors/Flot/jquery.flot.resize.js"></script>
	<!-- Flot plugins -->

	<script src="vendors/flot.orderbars/js/jquery.flot.orderBars.js"></script>
	<script src="vendors/flot-spline/js/jquery.flot.spline.min.js"></script>
	<script src="vendors/flot.curvedlines/curvedLines.js"></script>
	<!-- DateJS -->
	<script src="vendors/DateJS/build/date.js"></script>
	<!-- bootstrap-daterangepicker -->
	<script src="js/moment/moment.min.js"></script>
	<script src="js/datepicker/daterangepicker.js"></script>
	<script src="vendors/datatables.net/js/jquery.dataTables.min.js"></script>
	<script src="vendors/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
	<script
		src="vendors/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
	<script
		src="vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js"></script>
	<script src="vendors/datatables.net-buttons/js/buttons.flash.min.js"></script>
	<script src="vendors/datatables.net-buttons/js/buttons.html5.min.js"></script>
	<script src="vendors/datatables.net-buttons/js/buttons.print.min.js"></script>

	<script
		src="vendors/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js"></script>
	<script
		src="vendors/datatables.net-keytable/js/dataTables.keyTable.min.js"></script>
	<script
		src="vendors/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
	<script
		src="vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js"></script>
	<script
		src="vendors/datatables.net-scroller/js/datatables.scroller.min.js"></script>
	<script src="vendors/jszip/dist/jszip.min.js"></script>
	<script src="vendors/pdfmake/build/pdfmake.min.js"></script>
	<script src="vendors/pdfmake/build/vfs_fonts.js"></script>
	<!-- Select2 -->
	<script src="vendors/select2/dist/js/select2.full.min.js"></script>
	<!-- Custom Theme Scripts -->
	<script src="build/js/custom.min.js"></script>


	<script>
		$(document).ready(function() {
			var handleDataTableButtons = function() {
				if ($("#datatable-buttons").length) {
					$("#datatable-buttons").DataTable({
						dom : "Bfrtip",
						buttons : [ {
							extend : "copy",
							className : "btn-sm"
						}, {
							extend : "csv",
							className : "btn-sm"
						}, {
							extend : "pdf",
							className : "btn-sm"
						}, {
							extend : "pdfHtml5",
							className : "btn-sm"
						}, {
							extend : "print",
							className : "btn-sm"
						}, ],
						responsive : true
					});
				}
			};

			TableManageButtons = function() {
				"use strict";
				return {
					init : function() {
						handleDataTableButtons();
					}
				};
			}();

			$('#datatable').dataTable();

			$('#datatable-keytable').DataTable({
				keys : true
			});

			$('#datatable-responsive').DataTable();

			$('#datatable-scroller').DataTable({
				ajax : "js/datatables/json/scroller-demo.json",
				deferRender : true,
				scrollY : 380,
				scrollCollapse : true,
				scroller : true
			});

			$('#datatable-fixed-header').DataTable({
				fixedHeader : true
			});

			var $datatable = $('#datatable-checkbox');

			$datatable.dataTable({
				'order' : [ [ 1, 'asc' ] ],
				'columnDefs' : [ {
					orderable : false,
					targets : [ 0 ]
				} ]
			});
			$datatable.on('draw.dt', function() {
				$('input').iCheck({
					checkboxClass : 'icheckbox_flat-green'
				});
			});

			TableManageButtons.init();
		});
	</script>
	<!-- /Datatables -->
	<!-- Flot -->
	<script>
		$(document)
				.ready(
						function() {
							//define chart clolors ( you maybe add more colors if you want or flot will add it automatic )
							var chartColours = [ '#96CA59', '#3F97EB',
									'#72c380', '#6f7a8a', '#f7cb38', '#5a8022',
									'#2c7282' ];

							//generate random number for charts
							randNum = function() {
								return (Math.floor(Math.random()
										* (1 + 40 - 20))) + 20;
							};

							var d1 = [];
							//var d2 = [];

							//here we generate data for chart
							for (var i = 0; i < 30; i++) {
								d1.push([
										new Date(Date.today().add(i).days())
												.getTime(),
										randNum() + i + i + 10 ]);
								//    d2.push([new Date(Date.today().add(i).days()).getTime(), randNum()]);
							}

							var chartMinDate = d1[0][0]; //first day
							var chartMaxDate = d1[20][0]; //last day

							var tickSize = [ 1, "day" ];
							var tformat = "%d/%m/%y";

							//graph options
							var options = {
								grid : {
									show : true,
									aboveData : true,
									color : "#3f3f3f",
									labelMargin : 10,
									axisMargin : 0,
									borderWidth : 0,
									borderColor : null,
									minBorderMargin : 5,
									clickable : true,
									hoverable : true,
									autoHighlight : true,
									mouseActiveRadius : 100
								},
								series : {
									lines : {
										show : true,
										fill : true,
										lineWidth : 2,
										steps : false
									},
									points : {
										show : true,
										radius : 4.5,
										symbol : "circle",
										lineWidth : 3.0
									}
								},
								legend : {
									position : "ne",
									margin : [ 0, -25 ],
									noColumns : 0,
									labelBoxBorderColor : null,
									labelFormatter : function(label, series) {
										// just add some space to labes
										return label + '&nbsp;&nbsp;';
									},
									width : 40,
									height : 1
								},
								colors : chartColours,
								shadowSize : 0,
								tooltip : true, //activate tooltip
								tooltipOpts : {
									content : "%s: %y.0",
									xDateFormat : "%d/%m",
									shifts : {
										x : -30,
										y : -50
									},
									defaultTheme : false
								},
								yaxis : {
									min : 0
								},
								xaxis : {
									mode : "time",
									minTickSize : tickSize,
									timeformat : tformat,
									min : chartMinDate,
									max : chartMaxDate
								}
							};
							var plot = $.plot($("#placeholder33x"), [ {
								label : "Email Sent",
								data : d1,
								lines : {
									fillColor : "rgba(150, 202, 89, 0.12)"
								}, //#96CA59 rgba(150, 202, 89, 0.42)
								points : {
									fillColor : "#fff"
								}
							} ], options);
						});
	</script>
	<!-- /Flot -->

	<!-- jQuery Sparklines -->
	<script>
		$(document).ready(
				function() {
					$(".sparkline_one").sparkline(
							[ 2, 4, 3, 4, 5, 4, 5, 4, 3, 4, 5, 6, 4, 5, 6, 3,
									5, 4, 5, 4, 5, 4, 3, 4, 5, 6, 7, 5, 4, 3,
									5, 6 ], {
								type : 'bar',
								height : '125',
								barWidth : 13,
								colorMap : {
									'7' : '#a1a1a1'
								},
								barSpacing : 2,
								barColor : '#26B99A'
							});

					$(".sparkline11").sparkline(
							[ 2, 4, 3, 4, 5, 4, 5, 4, 3, 4, 6, 2, 4, 3, 4, 5,
									4, 5, 4, 3 ], {
								type : 'bar',
								height : '40',
								barWidth : 8,
								colorMap : {
									'7' : '#a1a1a1'
								},
								barSpacing : 2,
								barColor : '#26B99A'
							});

					$(".sparkline22").sparkline(
							[ 2, 4, 3, 4, 7, 5, 4, 3, 5, 6, 2, 4, 3, 4, 5, 4,
									5, 4, 3, 4, 6 ], {
								type : 'line',
								height : '40',
								width : '200',
								lineColor : '#26B99A',
								fillColor : '#ffffff',
								lineWidth : 3,
								spotColor : '#34495E',
								minSpotColor : '#34495E'
							});
				});
	</script>
	<!-- /jQuery Sparklines -->

	<!-- Doughnut Chart -->
	<script>
		$(document).ready(
				function() {
					var canvasDoughnut, options = {
						legend : false,
						responsive : false
					};

					new Chart(document.getElementById("canvas1i"), {
						type : 'doughnut',
						tooltipFillColor : "rgba(51, 51, 51, 0.55)",
						data : {
							labels : [ "Symbian", "Blackberry", "Other",
									"Android", "IOS" ],
							datasets : [ {
								data : [ 15, 20, 30, 10, 30 ],
								backgroundColor : [ "#BDC3C7", "#9B59B6",
										"#E74C3C", "#26B99A", "#3498DB" ],
								hoverBackgroundColor : [ "#CFD4D8", "#B370CF",
										"#E95E4F", "#36CAAB", "#49A9EA" ]

							} ]
						},
						options : options
					});

					new Chart(document.getElementById("canvas1i2"), {
						type : 'doughnut',
						tooltipFillColor : "rgba(51, 51, 51, 0.55)",
						data : {
							labels : [ "Symbian", "Blackberry", "Other",
									"Android", "IOS" ],
							datasets : [ {
								data : [ 15, 20, 30, 10, 30 ],
								backgroundColor : [ "#BDC3C7", "#9B59B6",
										"#E74C3C", "#26B99A", "#3498DB" ],
								hoverBackgroundColor : [ "#CFD4D8", "#B370CF",
										"#E95E4F", "#36CAAB", "#49A9EA" ]

							} ]
						},
						options : options
					});

					new Chart(document.getElementById("canvas1i3"), {
						type : 'doughnut',
						tooltipFillColor : "rgba(51, 51, 51, 0.55)",
						data : {
							labels : [ "Symbian", "Blackberry", "Other",
									"Android", "IOS" ],
							datasets : [ {
								data : [ 15, 20, 30, 10, 30 ],
								backgroundColor : [ "#BDC3C7", "#9B59B6",
										"#E74C3C", "#26B99A", "#3498DB" ],
								hoverBackgroundColor : [ "#CFD4D8", "#B370CF",
										"#E95E4F", "#36CAAB", "#49A9EA" ]

							} ]
						},
						options : options
					});
				});
	</script>
	<!-- /Doughnut Chart -->

	<!-- bootstrap-daterangepicker -->
	<script type="text/javascript">
		$(document)
				.ready(
						function() {

							var cb = function(start, end, label) {
								console.log(start.toISOString(), end
										.toISOString(), label);
								$('#reportrange span').html(
										start.format('MMMM D, YYYY') + ' - '
												+ end.format('MMMM D, YYYY'));
							};

							var optionSet1 = {
								startDate : moment().subtract(29, 'days'),
								endDate : moment(),
								minDate : '01/01/2012',
								maxDate : '12/31/2015',
								dateLimit : {
									days : 60
								},
								showDropdowns : true,
								showWeekNumbers : true,
								timePicker : false,
								timePickerIncrement : 1,
								timePicker12Hour : true,
								ranges : {
									'Today' : [ moment(), moment() ],
									'Yesterday' : [
											moment().subtract(1, 'days'),
											moment().subtract(1, 'days') ],
									'Last 7 Days' : [
											moment().subtract(6, 'days'),
											moment() ],
									'Last 30 Days' : [
											moment().subtract(29, 'days'),
											moment() ],
									'This Month' : [ moment().startOf('month'),
											moment().endOf('month') ],
									'Last Month' : [
											moment().subtract(1, 'month')
													.startOf('month'),
											moment().subtract(1, 'month')
													.endOf('month') ]
								},
								opens : 'left',
								buttonClasses : [ 'btn btn-default' ],
								applyClass : 'btn-small btn-primary',
								cancelClass : 'btn-small',
								format : 'MM/DD/YYYY',
								separator : ' to ',
								locale : {
									applyLabel : 'Submit',
									cancelLabel : 'Clear',
									fromLabel : 'From',
									toLabel : 'To',
									customRangeLabel : 'Custom',
									daysOfWeek : [ 'Su', 'Mo', 'Tu', 'We',
											'Th', 'Fr', 'Sa' ],
									monthNames : [ 'January', 'February',
											'March', 'April', 'May', 'June',
											'July', 'August', 'September',
											'October', 'November', 'December' ],
									firstDay : 1
								}
							};
							$('#reportrange span').html(
									moment().subtract(29, 'days').format(
											'MMMM D, YYYY')
											+ ' - '
											+ moment().format('MMMM D, YYYY'));
							$('#reportrange').daterangepicker(optionSet1, cb);
							$('#reportrange').on('show.daterangepicker',
									function() {
										console.log("show event fired");
									});
							$('#reportrange').on('hide.daterangepicker',
									function() {
										console.log("hide event fired");
									});
							$('#reportrange')
									.on(
											'apply.daterangepicker',
											function(ev, picker) {
												console
														.log("apply event fired, start/end dates are "
																+ picker.startDate
																		.format('MMMM D, YYYY')
																+ " to "
																+ picker.endDate
																		.format('MMMM D, YYYY'));
											});
							$('#reportrange').on('cancel.daterangepicker',
									function(ev, picker) {
										console.log("cancel event fired");
									});
							$('#options1').click(
									function() {
										$('#reportrange').data(
												'daterangepicker').setOptions(
												optionSet1, cb);
									});
							$('#options2').click(
									function() {
										$('#reportrange').data(
												'daterangepicker').setOptions(
												optionSet2, cb);
									});
							$('#destroy').click(
									function() {
										$('#reportrange').data(
												'daterangepicker').remove();
									});
						});
	</script>
	<!-- /bootstrap-daterangepicker -->


</body>
</html>